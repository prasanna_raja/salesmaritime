<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('user_roles', 'user_rolesController');
Route::resource('users', 'usersController');
Route::get('profile', 'usersController@getProfile');
Route::post('updateProfile', 'usersController@updateProfile');
Route::resource('country', 'CountryController');
// Route::resource('category', 'CategoryController');
// Route::resource('subcategory', 'SubcategoryController');
// Route::resource('subcategory', 'SubcategoryController');
Route::resource('job-category', 'JobCategoryController');
Route::resource('jobsub-category', 'JobsubCategoryController');

Route::get('jobsub-category/{id}/view', 'JobsubCategoryController@view');
Route::get('jobsub-category/{id}/add', 'JobsubCategoryController@add');
Route::post('jobsub-category/{id}/insert', 'JobsubCategoryController@insert');
Route::get('jobsub-category/{id}/edit', 'JobsubCategoryController@edit');

Route::resource('dir-category', 'DirCategoryController');
Route::resource('dirsub-category', 'DirsubCategoryController');

Route::get('dirsub-category/{id}/view', 'DirsubCategoryController@view');
Route::get('dirsub-category/{id}/add', 'DirsubCategoryController@add');
Route::post('dirsub-category/{id}/insert', 'DirsubCategoryController@insert');
Route::get('dirsub-category/{id}/edit', 'DirsubCategoryController@edit');


Route::resource('cranes', 'CranesController');
Route::post('cranes_item_del', 'CranesController@delete_item');
Route::resource('engines', 'EnginesController');
Route::post('engines_item_del', 'EnginesController@delete_item');
Route::resource('vessels', 'VesselsController');
Route::post('vessels_item_del', 'VesselsController@delete_item');
Route::resource('yachts', 'YachtsController');
Route::post('yachts_item_del', 'YachtsController@delete_item');
Route::resource('marine_equipment', 'Marine_equipmentController');
Route::post('mequip_item_del', 'Marine_equipmentController@delete_item');
// Route::resource('types', 'TypesController');

Route::resource('trash', 'TrashController');
Route::post('restore_item', 'TrashController@restore_item');
Route::post('per_del_item', 'TrashController@per_del_item');


Route::resource('categories', 'CategoriesController');
Route::resource('sub-categories', 'SubCategoriesController');
Route::get('sub-categories/{id}/view', 'SubCategoriesController@view');
Route::get('sub-categories/{id}/add', 'SubCategoriesController@add');
Route::post('sub-categories/{id}/insert', 'SubCategoriesController@insert');
Route::get('sub-categories/{id}/edit', 'SubCategoriesController@edit');


Route::resource('types', 'TypesController');
Route::resource('charters', 'ChartersController');
Route::resource('charter_inq', 'Charter_inqController');
Route::resource('other_inq', 'Other_inqController');

Route::resource('dir_items', 'dir_itemsController');
Route::resource('jobs', 'JobsController');
Route::resource('job-type', 'JobTypeController');
Route::resource('job-experience', 'JobExperienceController');

Route::resource('job-cv', 'JobCvController');
Route::resource('cv_education', 'cv_educationController');
Route::resource('cv_experience', 'cv_experienceController');
Route::resource('cv_launguage', 'cv_launguageController');
Route::resource('job_education', 'job_educationController');
Route::resource('tenders', 'TendersController');
Route::resource('sponsors-logo', 'SponsorsLogoController');
Route::resource('banners', 'BannersController');
Route::resource('news', 'NewsController');
Route::resource('file-storage', 'FileStorageController');
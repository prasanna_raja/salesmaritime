$(document).ready(function() {

	$(".del_item_vessels").click(function(){

		if(confirm("Are You Sure want to delete this item?")){

			var item_id = $(this).attr('id');
			var curr_user_id = $(".curr_user_id").val();
			console.log($(this).attr('id'));
			console.log(curr_user_id);
			$.ajax({
	            url: 'vessels_item_del',
	            type: 'post',
	            data: {item_id:item_id, user_id:curr_user_id},
	            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	            success: function(data){
	                console.log(data);
	                if(data == 1){
	                	alert("Successfully delete Item");
	                	location.reload();
	                }else{
	                	alert("Failed to delete Item");
	                	location.reload();
	                }
	            }
	            
	        });

		}else{

		}
		
		
	});

	$(".del_item_yachts").click(function(){

		if(confirm("Are You Sure want to delete this item?")){

			var item_id = $(this).attr('id');
			var curr_user_id = $(".curr_user_id").val();
			console.log($(this).attr('id'));
			console.log(curr_user_id);
			$.ajax({
	            url: 'yachts_item_del',
	            type: 'post',
	            data: {item_id:item_id, user_id:curr_user_id},
	            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	            success: function(data){
	                console.log(data);
	                if(data == 1){
	                	alert("Successfully delete Item");
	                	location.reload();
	                }else{
	                	alert("Failed to delete Item");
	                	location.reload();
	                }
	            }
	            
	        });

		}else{

		}
		
		
	});

	$(".del_item_mequip").click(function(){

		if(confirm("Are You Sure want to delete this item?")){

			var item_id = $(this).attr('id');
			var curr_user_id = $(".curr_user_id").val();
			console.log($(this).attr('id'));
			console.log(curr_user_id);
			$.ajax({
	            url: 'mequip_item_del',
	            type: 'post',
	            data: {item_id:item_id, user_id:curr_user_id},
	            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	            success: function(data){
	                console.log(data);
	                if(data == 1){
	                	alert("Successfully delete Item");
	                	location.reload();
	                }else{
	                	alert("Failed to delete Item");
	                	location.reload();
	                }
	            }
	            
	        });

		}else{

		}
		
		
	});

	$(".del_item_cranes").click(function(){

		if(confirm("Are You Sure want to delete this item?")){

			var item_id = $(this).attr('id');
			var curr_user_id = $(".curr_user_id").val();
			console.log($(this).attr('id'));
			console.log(curr_user_id);
			$.ajax({
	            url: 'cranes_item_del',
	            type: 'post',
	            data: {item_id:item_id, user_id:curr_user_id},
	            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	            success: function(data){
	                console.log(data);
	                if(data == 1){
	                	alert("Successfully delete Item");
	                	location.reload();
	                }else{
	                	alert("Failed to delete Item");
	                	location.reload();
	                }
	            }
	            
	        });

		}else{

		}
		
		
	});

	$(".del_item_engines").click(function(){

		if(confirm("Are You Sure want to delete this item?")){

			var item_id = $(this).attr('id');
			var curr_user_id = $(".curr_user_id").val();
			console.log($(this).attr('id'));
			console.log(curr_user_id);
			$.ajax({
	            url: 'engines_item_del',
	            type: 'post',
	            data: {item_id:item_id, user_id:curr_user_id},
	            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	            success: function(data){
	                console.log(data);
	                if(data == 1){
	                	alert("Successfully delete Item");
	                	location.reload();
	                }else{
	                	alert("Failed to delete Item");
	                	location.reload();
	                }
	            }
	            
	        });

		}else{

		}
		
		
	});

	/*Restore Item*/

	$(".restore_item").click(function(){

		//if(confirm("Are You Sure want to delete this item?")){

			var item_id = $(this).attr('id');
			var curr_user_id = $(".curr_user_id").val();
			var cat_id = $(".cat_id").val();
			console.log($(this).attr('id'));
			console.log(curr_user_id);
			console.log(cat_id);

			$.ajax({
	            url: 'restore_item',
	            type: 'post',
	            data: {item_id:item_id, user_id:curr_user_id, cat_id:cat_id},
	            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	            success: function(data){
	                console.log(data);
	                if(data == 1){
	                	alert("Successfully Restore Item");
	                	location.reload();
	                }else{
	                	alert("Failed to Restore Item");
	                	location.reload();
	                }
	            }
	            
	        });

		//}else{

		//}
		
		
	});

	$(".permanent_del").click(function(){

		if(confirm("Are You Sure want to permanent delete this item?")){

			var item_id = $(this).attr('id');
			var curr_user_id = $(".curr_user_id").val();
			var cat_id = $(".cat_id").val();
			console.log($(this).attr('id'));
			console.log(curr_user_id);
			console.log(cat_id);

			$.ajax({
	            url: 'per_del_item',
	            type: 'post',
	            data: {item_id:item_id, user_id:curr_user_id, cat_id:cat_id},
	            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	            success: function(data){
	                console.log(data);
	                if(data == 1){
	                	alert("Successfully Deleted Item");
	                	location.reload();
	                }else{
	                	alert("Failed to Delete Item");
	                	location.reload();
	                }
	            }
	            
	        });

		}else{

		}
		
		
	});
	

	

});
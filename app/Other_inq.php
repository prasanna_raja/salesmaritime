<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Other_inq extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'other_inqs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date_add', 'date', 'subject', 'description', 'status', 'user_id', 'deleted', 'delete_user_id'];

    
}

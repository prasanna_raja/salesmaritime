<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SponsorsLogo;
use Illuminate\Http\Request;
use Session;

class SponsorsLogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $sponsorslogo = SponsorsLogo::where('order', 'LIKE', "%$keyword%")
				->orWhere('photo', 'LIKE', "%$keyword%")
				->orWhere('user_id', 'LIKE', "%$keyword%")
				->orWhere('description', 'LIKE', "%$keyword%")
				->orWhere('date_add', 'LIKE', "%$keyword%")
				->orWhere('date_start', 'LIKE', "%$keyword%")
				->orWhere('date_expire', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->orWhere('url', 'LIKE', "%$keyword%")
				->orWhere('title', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $sponsorslogo = SponsorsLogo::paginate($perPage);
        }

        return view('sponsors-logo.index', compact('sponsorslogo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('sponsors-logo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
   

        if ($request->hasFile('photo')) {
            $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        
              $request->photo->move(public_path('images/photo'), $imageName);
                   
              $requestData['photo'] = $imageName;
        }

        SponsorsLogo::create($requestData);

        Session::flash('flash_message', 'SponsorsLogo added!');

        return redirect('sponsors-logo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $sponsorslogo = SponsorsLogo::findOrFail($id);

        return view('sponsors-logo.show', compact('sponsorslogo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $sponsorslogo = SponsorsLogo::findOrFail($id);

        return view('sponsors-logo.edit', compact('sponsorslogo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

        if ($request->hasFile('photo')) {
            $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        
              $request->photo->move(public_path('images/photo'), $imageName);
                   
              $requestData['photo'] = $imageName;
        }

        $sponsorslogo = SponsorsLogo::findOrFail($id);
        $sponsorslogo->update($requestData);

        Session::flash('flash_message', 'SponsorsLogo updated!');

        return redirect('sponsors-logo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        SponsorsLogo::destroy($id);

        Session::flash('flash_message', 'SponsorsLogo deleted!');

        return redirect('sponsors-logo');
    }
}

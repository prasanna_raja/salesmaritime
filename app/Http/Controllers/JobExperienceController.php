<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\JobExperience;
use Illuminate\Http\Request;
use Session;

class JobExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jobexperience = JobExperience::where('job_exp_name', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $jobexperience = JobExperience::paginate($perPage);
        }

        return view('job-experience.index', compact('jobexperience'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('job-experience.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        JobExperience::create($requestData);

        Session::flash('flash_message', 'JobExperience added!');

        return redirect('job-experience');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $jobexperience = JobExperience::findOrFail($id);

        return view('job-experience.show', compact('jobexperience'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $jobexperience = JobExperience::findOrFail($id);

        return view('job-experience.edit', compact('jobexperience'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $jobexperience = JobExperience::findOrFail($id);
        $jobexperience->update($requestData);

        Session::flash('flash_message', 'JobExperience updated!');

        return redirect('job-experience');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        JobExperience::destroy($id);

        Session::flash('flash_message', 'JobExperience deleted!');

        return redirect('job-experience');
    }
}

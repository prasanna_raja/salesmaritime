<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Charter;
use Illuminate\Http\Request;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;

class ChartersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        $type_id = $request->get('type_id');
        $cat_id = $request->get('cat_id');

        if (!empty($keyword)) {
            $charters = Charter::where('category_id', 'LIKE', "%$keyword%")
				->orWhere('sub_category_id', 'LIKE', "%$keyword%")
				->orWhere('photo', 'LIKE', "%$keyword%")
				->orWhere('protocol_file', 'LIKE', "%$keyword%")
				->orWhere('user_id', 'LIKE', "%$keyword%")
				->orWhere('description', 'LIKE', "%$keyword%")
				->orWhere('date_add', 'LIKE', "%$keyword%")
				->orWhere('date_del', 'LIKE', "%$keyword%")
				->orWhere('date_start', 'LIKE', "%$keyword%")
				->orWhere('date_end', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->orWhere('purchase', 'LIKE', "%$keyword%")
				->orWhere('sold', 'LIKE', "%$keyword%")
				->orWhere('year_of_build', 'LIKE', "%$keyword%")
				->orWhere('country_build', 'LIKE', "%$keyword%")
				->orWhere('name', 'LIKE', "%$keyword%")
				->orWhere('condition', 'LIKE', "%$keyword%")
				->orWhere('location', 'LIKE', "%$keyword%")
				->orWhere('class', 'LIKE', "%$keyword%")
				->orWhere('imo_number', 'LIKE', "%$keyword%")
				->orWhere('crews', 'LIKE', "%$keyword%")
				->orWhere('propellers', 'LIKE', "%$keyword%")
				->orWhere('knote', 'LIKE', "%$keyword%")
				->orWhere('location_from', 'LIKE', "%$keyword%")
				->orWhere('location_to', 'LIKE', "%$keyword%")
				->orWhere('hp', 'LIKE', "%$keyword%")
				->orWhere('measurement', 'LIKE', "%$keyword%")
				->orWhere('length', 'LIKE', "%$keyword%")
				->orWhere('breadth', 'LIKE', "%$keyword%")
				->orWhere('depth', 'LIKE', "%$keyword%")
				->orWhere('tonnage_net', 'LIKE', "%$keyword%")
				->orWhere('tonnage_gross', 'LIKE', "%$keyword%")
				->orWhere('light_weight', 'LIKE', "%$keyword%")
				->orWhere('dead_weight', 'LIKE', "%$keyword%")
				->orWhere('price', 'LIKE', "%$keyword%")
				->orWhere('deleted', 'LIKE', "%$keyword%")
				->orWhere('delete_user_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {

            $charters = Charter::where([ ['type_id', '=', $type_id], ['category_id', '=', $cat_id] ])->leftjoin('users','charters.user_id', '=', 'users.id')
            ->select('charters.id', 'charters.date_add', 'charters.deleted', 'charters.photo', 'charters.year_of_build', 'charters.location', 'charters.description', 'charters.price', 'charters.user_id', 'charters.status', 'charters.sold', 'users.first_name', 'users.last_name')
            ->paginate($perPage);

           // $charters = Charter::paginate($perPage);
        }

        return view('charters.index', ['charters'=>$charters, 'type_id'=>$type_id, 'cat_id'=>$cat_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $user_id = Auth::id();
        if($user_id != ''){

            $type_id = $request->get('type_id');
            $cat_id = $request->get('cat_id');
            $categories = DB::table('categories')->where([ ['type', '=', 1],['parent_id', '=', $type_id] ])->get();

            $sub_categories = DB::table('categories')->where([ ['type', '=', 2], ['parent_id', '=', $cat_id] ])->get();
            $countries = DB::table('countries')->get();
            
            return view('charters.create', ['categories'=>$categories, 'sub_categories'=>$sub_categories, 'user_id' => $user_id, 'countries'=>$countries, 'type_id'=>$type_id, 'cat_id'=>$cat_id]);
        }else{
            return view('auth.login');
        }
       // return view('charters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $type_id = $request->get('type_id'); 

        $cat_id = $request->get('category_id'); 

        if ($request->hasFile('photo')) {
            $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        
              $request->photo->move(public_path('images/photo'), $imageName);
                   
              $requestData['photo'] = $imageName;
        }


        if ($request->hasFile('protocol_file')) {
            $this->validate($request, [
            'protocol_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName1 = time().'.'.$request->protocol_file->getClientOriginalExtension();
        
              $request->protocol_file->move(public_path('images/protocol_file'), $imageName1);
                   
              $requestData['protocol_file'] = $imageName1;
        }

        Charter::create($requestData);

        Session::flash('flash_message', 'Charter added!');

        return redirect('charters?type_id='.$type_id.'&cat_id='.$cat_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $charter = Charter::findOrFail($id);

        $type_id = $request->get('type_id');
        $cat_id = $request->get('cat_id'); 

        return view('charters.show', compact('charter', 'type_id', 'cat_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $charter = Charter::findOrFail($id);

        $type_id = $request->get('type_id'); 

        $cat_id = $request->get('cat_id'); 

        $categories = DB::table('categories')->where([ ['type', '=', 1],['parent_id', '=', $type_id] ])->get();

        $sub_categories = DB::table('categories')->where([ ['type', '=', 2], ['parent_id', '=', $cat_id] ])->get();
        $countries = DB::table('countries')->get();

        return view('charters.edit', ['charter'=>$charter, 'categories'=>$categories, 'sub_categories'=>$sub_categories, 'user_id'=>$id, 'countries'=>$countries, 'type_id'=>$type_id, 'cat_id'=>$cat_id]);

        //return view('charters.edit', compact('charter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

        if ($request->hasFile('photo')) {
            $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        
              $request->photo->move(public_path('images/photo'), $imageName);
                   
              $requestData['photo'] = $imageName;
        }


        if ($request->hasFile('protocol_file')) {
            $this->validate($request, [
            'protocol_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName1 = time().'.'.$request->protocol_file->getClientOriginalExtension();
        
              $request->protocol_file->move(public_path('images/protocol_file'), $imageName1);
                   
              $requestData['protocol_file'] = $imageName1;
        }

        $charter = Charter::findOrFail($id);

        $type_id = $requestData['type_id'];

        $cat_id = $requestData['category_id'];

        $charter->update($requestData);

        Session::flash('flash_message', 'Charter updated!');

        return redirect('charters?type_id='.$type_id.'&cat_id='.$cat_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, Request $request)
    {
        Charter::destroy($id);

        $type_id = $request->get('type_id'); 

        $cat_id = $request->get('cat_id');

        Session::flash('flash_message', 'Charter deleted!');

        return redirect('charters?type_id='.$type_id.'&cat_id='.$cat_id);
    }
}

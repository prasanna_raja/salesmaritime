<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $get_cat = DB::table('categories')->where('parent_id', '=', 0)->get();

        // Sharing is caring
        View::share('get_cat', $get_cat);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\cv_education;
use Illuminate\Http\Request;
use Session;

class cv_educationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cv_education = cv_education::where('cv_id', 'LIKE', "%$keyword%")
				->orWhere('education_id', 'LIKE', "%$keyword%")
				->orWhere('name', 'LIKE', "%$keyword%")
				->orWhere('department', 'LIKE', "%$keyword%")
				->orWhere('dip_deg', 'LIKE', "%$keyword%")
				->orWhere('year_end', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $cv_education = cv_education::paginate($perPage);
        }

        return view('cv_education.index', compact('cv_education'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cv_education.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        cv_education::create($requestData);

        Session::flash('flash_message', 'cv_education added!');

        return redirect('cv_education');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $cv_education = cv_education::findOrFail($id);

        return view('cv_education.show', compact('cv_education'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cv_education = cv_education::findOrFail($id);

        return view('cv_education.edit', compact('cv_education'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $cv_education = cv_education::findOrFail($id);
        $cv_education->update($requestData);

        Session::flash('flash_message', 'cv_education updated!');

        return redirect('cv_education');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        cv_education::destroy($id);

        Session::flash('flash_message', 'cv_education deleted!');

        return redirect('cv_education');
    }
}

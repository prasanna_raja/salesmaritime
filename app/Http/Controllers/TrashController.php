<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Vessel;
use Illuminate\Http\Request;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;

class TrashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
       
        $user_id = Auth::id();
        if($user_id != ''){

            $cat_id = $request->get('cat_id');

            if($cat_id == 1){
                $t_name = 'vessels';
            }elseif($cat_id == 2){
                $t_name = 'yachts';
            }elseif($cat_id == 3){
                $t_name = 'marine_equipments';
            }elseif($cat_id == 4){
                $t_name = 'engines';
            }elseif($cat_id == 5){
                $t_name = 'cranes';
            }else{
                $t_name = 0;
            }

            
            if($t_name != '0'){

                $perPage = 25;

                $count_ves = DB::table('vessels')->where([ ['deleted', '=', 1] ])->count();
                $count_yac = DB::table('yachts')->where([ ['deleted', '=', 1] ])->count();
                $count_cra = DB::table('cranes')->where([ ['deleted', '=', 1] ])->count();
                $count_mar = DB::table('marine_equipments')->where([ ['deleted', '=', 1] ])->count();
                $count_eng = DB::table('engines')->where([ ['deleted', '=', 1] ])->count();

                $trashes = DB::table($t_name)->where([ ['deleted', '=', 1] ])->paginate($perPage);

                return view('trash.index', ['trashes'=>$trashes, 'cat_id'=>$cat_id, 'user_id'=>$user_id, 'count_ves'=>$count_ves, 'count_yac'=>$count_yac, 'count_cra'=>$count_cra, 'count_mar'=>$count_mar, 'count_eng'=>$count_eng]);

            }else{

                return view('home');
            }

            

        }else{
            return view('auth.login');
        }
        
    }

    public function restore_item(Request $request){

        $cat_id = $request->get('cat_id');
        $item_id = $request->get('item_id');

        if($cat_id == 1){
            $t_name = 'vessels';
        }elseif($cat_id == 2){
            $t_name = 'yachts';
        }elseif($cat_id == 3){
            $t_name = 'marine_equipments';
        }elseif($cat_id == 4){
            $t_name = 'engines';
        }elseif($cat_id == 5){
            $t_name = 'cranes';
        }else{
            $t_name = 0;
        }

        if($t_name != '0'){

            $trashes = DB::table($t_name)->where('id', $item_id)->update( ['deleted' => 0] );
            if($trashes == 1){
                echo 1;
            }else{
                echo 0;  
            }
            

        }else{
            echo 0; 
        }

    }

    public function per_del_item(Request $request){

        $cat_id = $request->get('cat_id');
        $item_id = $request->get('item_id');

        if($cat_id == 1){
            $t_name = 'vessels';
        }elseif($cat_id == 2){
            $t_name = 'yachts';
        }elseif($cat_id == 3){
            $t_name = 'marine_equipments';
        }elseif($cat_id == 4){
            $t_name = 'engines';
        }elseif($cat_id == 5){
            $t_name = 'cranes';
        }else{
            $t_name = 0;
        }

        if($t_name != '0'){

            $trashes = DB::table($t_name)->where('id', $item_id)->delete();
            if($trashes == 1){
                echo 1;
            }else{
                echo 0;  
            }
            

        }else{
            echo 0; 
        }

    }

    


}

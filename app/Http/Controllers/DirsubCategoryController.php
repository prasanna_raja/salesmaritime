<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DirCategory;
use Illuminate\Http\Request;
use Session;
use DB;

class DirsubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $dirsubcategory = DirCategory::where('parent_id', 'LIKE', "%$keyword%")
				->orWhere('dir_category_name', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $dirsubcategory = DirCategory::paginate($perPage);
        }

        return view('dirsub-category.index', compact('dirsubcategory'));
    }

    public function view($id, Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 25;

            
        if (!empty($keyword)) {
            $dirsubcategory = DirCategory::where('parent_id', 'LIKE', "%$keyword%")
                ->orWhere('dir_category_name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $dirsubcategory = DirCategory::where('parent_id','=',$id)->paginate($perPage);
        }
        

        return view('dirsub-category.index', ['dirsubcategory'=>$dirsubcategory, 'id'=>$id]);
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('dirsub-category.create');
    }

    public function add($id)
    {
        $get_categories = DB::table('dir_categories')->where('id', '=', $id)->get();

        foreach ($get_categories as $get_category) {
           $arr['dir_category_name'] = $get_category->dir_category_name;
           $arr['parent_id'] = $get_category->parent_id;
           $parent_id = $get_category->parent_id;
            $arr['parent_id'] = $get_category->id;

           $parents = DB::table('dir_categories')->where('id', '=', $parent_id)->get();
           $arr['type_name'] = 'No';
           foreach ($parents as $parent) {
                $arr['type_name'] = $parent->dir_category_name;
           }
        }

        return view('dirsub-category.create', ['get_categories'=>$arr, 'id'=>$id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        DirCategory::create($requestData);

        Session::flash('flash_message', 'DirsubCategory added!');

        return redirect('dirsub-category');
    }

    public function insert($id, Request $request)
    {
        
        $requestData = $request->all();
        

        //print_r($requestData);exit;

        DirCategory::create($requestData);

        Session::flash('flash_message', 'SubCategory added!');

        return redirect('dirsub-category/'.$id.'/view');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $dirsubcategory = DirCategory::findOrFail($id);

        return view('dirsub-category.show', compact('dirsubcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($s_id, request $request)
    {
        $req = $request->all();

        $id = $req['parent_id'];

        $get_categories = DB::table('dir_categories')->where('id', '=', $id)->get();

        foreach ($get_categories as $get_category) {
           $arr['dir_category_name'] = $get_category->dir_category_name;
           $arr['parent_id'] = $get_category->parent_id;
           $parent_id = $get_category->parent_id;
            $arr['parent_id'] = $get_category->id;

           $parents = DB::table('dir_categories')->where('id', '=', $parent_id)->get();
           $arr['type_name'] = 'No';
           foreach ($parents as $parent) {
                $arr['type_name'] = $parent->dir_category_name;
           }
        }

        $dirsubcategory = DirCategory::findOrFail($s_id);

        //return view('dirsub-category.edit', compact('dirsubcategory'));
        return view('dirsub-category.edit', ['get_categories'=>$arr, 'dirsubcategory'=>$dirsubcategory, 'id'=>$id]);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $dirsubcategory = DirCategory::findOrFail($id);
        $parents = DB::table('dir_categories')->where('id', '=', $id)->get();
        foreach ($parents as $parent) {
            $parent_id = $parent->parent_id;
        }
        $dirsubcategory->update($requestData);

        Session::flash('flash_message', 'DirsubCategory updated!');

        return redirect('dirsub-category/'.$parent_id.'/view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $parents = DB::table('dir_categories')->where('id', '=', $id)->get();
        foreach ($parents as $parent) {
            $parent_id = $parent->parent_id;
        }
        
        DirCategory::destroy($id);

        Session::flash('flash_message', 'DirsubCategory deleted!');

        return redirect('dirsub-category/'.$parent_id.'/view');
    }
}

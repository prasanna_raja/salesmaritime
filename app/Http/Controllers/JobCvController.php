<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\JobCv;
use Illuminate\Http\Request;
use Session;
use DB;
use Auth;

class JobCvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jobcv = JobCv::where('user_id', 'LIKE', "%$keyword%")
				->orWhere('job_category_id', 'LIKE', "%$keyword%")
				->orWhere('jobsub_category_id', 'LIKE', "%$keyword%")
				->orWhere('title', 'LIKE', "%$keyword%")
				->orWhere('first_name', 'LIKE', "%$keyword%")
				->orWhere('last_name', 'LIKE', "%$keyword%")
				->orWhere('gender', 'LIKE', "%$keyword%")
				->orWhere('date_add', 'LIKE', "%$keyword%")
				->orWhere('date_expire', 'LIKE', "%$keyword%")
				->orWhere('date_birth', 'LIKE', "%$keyword%")
				->orWhere('need_salary', 'LIKE', "%$keyword%")
				->orWhere('email', 'LIKE', "%$keyword%")
				->orWhere('mobile', 'LIKE', "%$keyword%")
				->orWhere('country', 'LIKE', "%$keyword%")
				->orWhere('nationality', 'LIKE', "%$keyword%")
				->orWhere('city', 'LIKE', "%$keyword%")
				->orWhere('reloaction', 'LIKE', "%$keyword%")
				->orWhere('lang_nat', 'LIKE', "%$keyword%")
				->orWhere('cv_file', 'LIKE', "%$keyword%")
				->orWhere('photo', 'LIKE', "%$keyword%")
				->orWhere('deleted', 'LIKE', "%$keyword%")
				->orWhere('date_del', 'LIKE', "%$keyword%")
				->orWhere('delete_user_id', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $jobcv = JobCv::paginate($perPage);
        }

        return view('job-cv.index', compact('jobcv'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user_id = Auth::id();
        if($user_id != ''){
            $job_categories = DB::table('job_categories')->where('parent_id', '=', 0)->get();
            $jobsub_categories = DB::table('job_categories')->where('parent_id', '!=', 0)->get();
            $users = DB::table('users')->where('id', '=', $user_id)->get();
            $countries = DB::table('countries')->get();
            $educations = DB::table('job_educations')->get();
            return view('job-cv.create', ['user_id'=>$user_id, 'users'=>$users, 'job_categories'=>$job_categories, 'jobsub_categories'=>$jobsub_categories, 'countries'=>$countries, 'educations'=>$educations]);
        }else{
            return view('auth.login');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        //print_r($requestData);exit;
        
        if ($request->hasFile('photo')) {
            $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        
              $request->photo->move(public_path('images/photo'), $imageName);
                   
              $requestData['photo'] = $imageName;
        }


        if ($request->hasFile('cv_file')) {
            $this->validate($request, [
            'cv_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName1 = time().'.'.$request->cv_file->getClientOriginalExtension();
        
              $request->cv_file->move(public_path('images/cv_file'), $imageName1);
                   
              $requestData['cv_file'] = $imageName1;

        }else{

             $requestData['cv_file'] = ''; 
        }

        $user_id = $requestData['user_id'];
        $job_category_id = $requestData['job_category_id'];
        $jobsub_category_id = $requestData['jobsub_category_id'];
        $title = $requestData['title'];
        $first_name = $requestData['first_name'];
        $last_name = $requestData['last_name'];
        $gender = $requestData['gender'];
        $date_add = $requestData['date_add'];
        $date_expire = $requestData['date_expire'];
        $date_birth = $requestData['date_birth'];
        $need_salary = $requestData['need_salary'];
        $email = $requestData['email'];
        $mobile = $requestData['mobile'];
        $country = $requestData['country'];
        $nationality = $requestData['nationality'];
        $city = $requestData['city'];
        $lang_nat = $requestData['lang_nat'];
        $reloaction = $requestData['reloaction'];
        $photo = $requestData['photo'];
        $cv_file = $requestData['cv_file'];

        


        $cv_id = DB::table('job_cvs')->insertGetId(
            array('user_id' => $user_id, 'job_category_id' => $job_category_id, 'jobsub_category_id'=>$jobsub_category_id, 'title'=>$title, 'first_name'=>$first_name, 'last_name'=>$last_name, 'gender'=>$gender, 'date_add'=>$date_add, 'date_expire'=>$date_expire, 'date_birth'=>$date_birth, 'need_salary'=>$need_salary, 'email'=>$email, 'mobile'=>$mobile, 'country'=>$country, 'nationality'=>$nationality, 'city'=>$city, 'lang_nat'=>$lang_nat, 'reloaction'=>$reloaction, 'photo'=>$photo, 'cv_file'=>$cv_file)
        );

        $educations = $requestData['education_id'];
        $name = $requestData['name'];
        $department = $requestData['department'];
        $dip_deg = $requestData['dip_deg'];
        $year_end = $requestData['year_end'];

        foreach ($educations as $k => $v) {
            
            $edu_s = DB::table('cv_educations')->insertGetId(
                array('cv_id' => $cv_id, 'education_id'=>$v, 'name'=>$name[$k], 'department'=>$department[$k], 'dip_deg'=>$dip_deg[$k], 'year_end'=>$year_end[$k])
            );

        }


        $companies = $requestData['company'];
        $position = $requestData['position'];
        $responsibility = $requestData['responsibility'];
        $start_work = $requestData['start_work'];
        $end_work = $requestData['end_work'];

        foreach ($companies as $key => $company) {
            
            $exp_s = DB::table('cv_experiences')->insertGetId(
                array('cv_id' => $cv_id, 'company'=>$company, 'position'=>$position[$key], 'responsibility'=>$responsibility[$key], 'start_work'=>$start_work[$key], 'end_work'=>$end_work[$key])
            );
        }

        $languages = $requestData['lang'];
        $level = $requestData['level'];

        foreach ($languages as $lang => $language) {
            
            $lang_s = DB::table('cv_launguages')->insertGetId(
                array('cv_id' => $cv_id, 'lang'=>$language, 'level'=>$level[$lang])
            );
        }


        //JobCv::create($requestData);

        Session::flash('flash_message', 'JobCv added!');

        return redirect('job-cv');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $jobcv = JobCv::findOrFail($id);

        return view('job-cv.show', compact('jobcv'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user_id = Auth::id();
        if($user_id != ''){
            
            $job_categories = DB::table('job_categories')->where('parent_id', '=', 0)->get();
            $jobsub_categories = DB::table('job_categories')->where('parent_id', '!=', 0)->get();
            $users = DB::table('users')->where('id', '=', $user_id)->get();
            $countries = DB::table('countries')->get();
            $educations = DB::table('job_educations')->get();

            
            $jobcv = JobCv::findOrFail($id); //DB::table('job_cvs')->where('id', '=', $id)->get();

            $cv_educations = DB::table('cv_educations')->where('cv_id', '=', $id)->get();
            $cv_experiences = DB::table('cv_experiences')->where('cv_id', '=', $id)->get();
            $cv_launguages = DB::table('cv_launguages')->where('cv_id', '=', $id)->get();

            // print_r($cv_educations);
            // print_r($cv_experiences);
            // print_r($cv_launguages);exit;


            return view('job-cv.edit', compact('jobcv', 'job_categories', 'jobsub_categories', 'users', 'countries', 'user_id', 'educations', 'cv_educations', 'cv_experiences', 'cv_launguages'));
        }else{
            return view('auth.login');
        }    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();

        print_r($requestData);//exit;
        

        if ($request->hasFile('photo')) {
            $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        
              $request->photo->move(public_path('images/photo'), $imageName);
                   
              $requestData['photo'] = $imageName;
        }


        if ($request->hasFile('cv_file')) {
            $this->validate($request, [
            'cv_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName1 = time().'.'.$request->cv_file->getClientOriginalExtension();
        
              $request->cv_file->move(public_path('images/cv_file'), $imageName1);
                   
              $requestData['cv_file'] = $imageName1;
        }

        $user_id = $requestData['user_id'];
        $job_category_id = $requestData['job_category_id'];
        $jobsub_category_id = $requestData['jobsub_category_id'];
        $title = $requestData['title'];
        $first_name = $requestData['first_name'];
        $last_name = $requestData['last_name'];
        $gender = $requestData['gender'];
        $date_add = $requestData['date_add'];
        $date_expire = $requestData['date_expire'];
        $date_birth = $requestData['date_birth'];
        $need_salary = $requestData['need_salary'];
        $email = $requestData['email'];
        $mobile = $requestData['mobile'];
        $country = $requestData['country'];
        $nationality = $requestData['nationality'];
        $city = $requestData['city'];
        $lang_nat = $requestData['lang_nat'];
        $reloaction = $requestData['reloaction'];
        if(isset($requestData['photo'])){
            $photo = $requestData['photo'];
        }else{
            $photo = '';
        }

        if(isset($requestData['cv_file'])){
            $cv_file = $requestData['cv_file'];
        }else{
            $cv_file = '';
        }
        


        $update_cv = DB::table('job_cvs')->where('id', '=', $id)->update(
            array('user_id' => $user_id, 'job_category_id' => $job_category_id, 'jobsub_category_id'=>$jobsub_category_id, 'title'=>$title, 'first_name'=>$first_name, 'last_name'=>$last_name, 'gender'=>$gender, 'date_add'=>$date_add, 'date_expire'=>$date_expire, 'date_birth'=>$date_birth, 'need_salary'=>$need_salary, 'email'=>$email, 'mobile'=>$mobile, 'country'=>$country, 'nationality'=>$nationality, 'city'=>$city, 'lang_nat'=>$lang_nat, 'reloaction'=>$reloaction, 'photo'=>$photo, 'cv_file'=>$cv_file)
        );

        
        $cv_id = $id;

        $educations = $requestData['education_id'];
        $name = $requestData['name'];
        $department = $requestData['department'];
        $dip_deg = $requestData['dip_deg'];
        $year_end = $requestData['year_end'];

        $delete_cv_edu = DB::table('cv_educations')->where('cv_id', '=', $id)->delete();



        foreach ($educations as $k => $v) {
            
            $edu_s = DB::table('cv_educations')->insertGetId(
                array('cv_id' => $cv_id, 'education_id'=>$v, 'name'=>$name[$k], 'department'=>$department[$k], 'dip_deg'=>$dip_deg[$k], 'year_end'=>$year_end[$k])
            );

        }


        $companies = $requestData['company'];
        $position = $requestData['position'];
        $responsibility = $requestData['responsibility'];
        $start_work = $requestData['start_work'];
        $end_work = $requestData['end_work'];

        $delete_cv_exp = DB::table('cv_experiences')->where('cv_id', '=', $id)->delete();

        foreach ($companies as $key => $company) {
            
            $exp_s = DB::table('cv_experiences')->insertGetId(
                array('cv_id' => $cv_id, 'company'=>$company, 'position'=>$position[$key], 'responsibility'=>$responsibility[$key], 'start_work'=>$start_work[$key], 'end_work'=>$end_work[$key])
            );
        }

        $languages = $requestData['lang'];
        $level = $requestData['level'];

        $delete_cv_lang = DB::table('cv_launguages')->where('cv_id', '=', $id)->delete();

        foreach ($languages as $lang => $language) {
            
            $lang_s = DB::table('cv_launguages')->insertGetId(
                array('cv_id' => $cv_id, 'lang'=>$language, 'level'=>$level[$lang])
            );
        }


        // $jobcv = JobCv::findOrFail($id);



        // $jobcv->update($requestData);

        Session::flash('flash_message', 'JobCv updated!');

        return redirect('job-cv');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        JobCv::destroy($id);

        Session::flash('flash_message', 'JobCv deleted!');

        return redirect('job-cv');
    }
}

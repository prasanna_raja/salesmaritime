<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Engine;
use Illuminate\Http\Request;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;

class EnginesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user_id = Auth::id();
        if($user_id != ''){

            $keyword = $request->get('search');
            $type_id = $request->get('type_id');
            $cat_id = $request->get('cat_id');
            $perPage = 25;

            if (!empty($keyword)) {
                $engines = Engine::where('category_id', 'LIKE', "%$keyword%")
    				->orWhere('sub_category_id', 'LIKE', "%$keyword%")
    				->orWhere('photo', 'LIKE', "%$keyword%")
    				->orWhere('protocol_file', 'LIKE', "%$keyword%")
    				->orWhere('user_id', 'LIKE', "%$keyword%")
    				->orWhere('description', 'LIKE', "%$keyword%")
    				->orWhere('date_add', 'LIKE', "%$keyword%")
    				->orWhere('date_del', 'LIKE', "%$keyword%")
    				->orWhere('status', 'LIKE', "%$keyword%")
    				->orWhere('purchase', 'LIKE', "%$keyword%")
    				->orWhere('sold', 'LIKE', "%$keyword%")
    				->orWhere('year_of_build', 'LIKE', "%$keyword%")
    				->orWhere('country_build', 'LIKE', "%$keyword%")
    				->orWhere('name', 'LIKE', "%$keyword%")
    				->orWhere('model', 'LIKE', "%$keyword%")
    				->orWhere('make', 'LIKE', "%$keyword%")
    				->orWhere('serial_number', 'LIKE', "%$keyword%")
    				->orWhere('part_number', 'LIKE', "%$keyword%")
    				->orWhere('rpm', 'LIKE', "%$keyword%")
    				->orWhere('kw', 'LIKE', "%$keyword%")
    				->orWhere('kva', 'LIKE', "%$keyword%")
    				->orWhere('hz', 'LIKE', "%$keyword%")
    				->orWhere('condition', 'LIKE', "%$keyword%")
    				->orWhere('location', 'LIKE', "%$keyword%")
    				->orWhere('price', 'LIKE', "%$keyword%")
    				->orWhere('deleted', 'LIKE', "%$keyword%")
    				->orWhere('delete_user_id', 'LIKE', "%$keyword%")
    				->paginate($perPage);
            } else {

                $engines = Engine::where([ ['type_id', '=', $type_id], ['category_id', '=', $cat_id] ])->leftjoin('users','engines.user_id', '=', 'users.id')
                ->select('engines.id', 'engines.date_add', 'engines.deleted', 'engines.photo', 'engines.year_of_build', 'engines.location', 'engines.description', 'engines.price', 'engines.user_id', 'engines.status', 'engines.sold', 'users.first_name', 'users.last_name')
                ->paginate($perPage);

                // $engines = Engine::paginate($perPage);
            }

            return view('engines.index', ['engines'=>$engines, 'type_id'=>$type_id, 'cat_id'=>$cat_id, 'user_id'=>$user_id]);
        }else{
            return view('auth.login');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $user_id = Auth::id();
        if($user_id != ''){

            $type_id = $request->get('type_id');
            $cat_id = $request->get('cat_id');
            $categories = DB::table('categories')->where([ ['type', '=', 1],['parent_id', '=', $type_id] ])->get();

            $sub_categories = DB::table('categories')->where([ ['type', '=', 2], ['parent_id', '=', $cat_id] ])->get();
            $countries = DB::table('countries')->get();
            
            return view('engines.create', ['categories'=>$categories, 'sub_categories'=>$sub_categories, 'user_id' => $user_id, 'countries'=>$countries, 'type_id'=>$type_id, 'cat_id'=>$cat_id]);
        }else{
            return view('auth.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $type_id = $request->get('type_id'); 

        $cat_id = $request->get('category_id'); 

        if ($request->hasFile('photo')) {
            $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        
              $request->photo->move(public_path('images/photo'), $imageName);
                   
              $requestData['photo'] = $imageName;
        }


        if ($request->hasFile('protocol_file')) {
            $this->validate($request, [
            'protocol_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName1 = time().'.'.$request->protocol_file->getClientOriginalExtension();
        
              $request->protocol_file->move(public_path('images/protocol_file'), $imageName1);
                   
              $requestData['protocol_file'] = $imageName1;
        }

        Engine::create($requestData);

        Session::flash('flash_message', 'Engine added!');

        return redirect('engines?type_id='.$type_id.'&cat_id='.$cat_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id, Request $request)
    {

        $type_id = $request->get('type_id');
        $cat_id = $request->get('cat_id');

        $engine = Engine::findOrFail($id);

        return view('engines.show', compact('engine', 'type_id', 'cat_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $engine = Engine::findOrFail($id);

        $type_id = $request->get('type_id'); 

        $cat_id = $request->get('cat_id'); 

        $categories = DB::table('categories')->where([ ['type', '=', 1],['parent_id', '=', $type_id] ])->get();

        $sub_categories = DB::table('categories')->where([ ['type', '=', 2], ['parent_id', '=', $cat_id] ])->get();
        $countries = DB::table('countries')->get();

        return view('engines.edit', ['engine'=>$engine, 'categories'=>$categories, 'sub_categories'=>$sub_categories, 'user_id'=>$id, 'countries'=>$countries, 'type_id'=>$type_id, 'cat_id'=>$cat_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

        if ($request->hasFile('photo')) {
            $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        
              $request->photo->move(public_path('images/photo'), $imageName);
                   
              $requestData['photo'] = $imageName;
        }


        if ($request->hasFile('protocol_file')) {
            $this->validate($request, [
            'protocol_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName1 = time().'.'.$request->protocol_file->getClientOriginalExtension();
        
              $request->protocol_file->move(public_path('images/protocol_file'), $imageName1);
                   
              $requestData['protocol_file'] = $imageName1;
        }

        $engine = Engine::findOrFail($id);

        $type_id = $requestData['type_id'];

        $cat_id = $requestData['category_id'];

        $engine->update($requestData);


        Session::flash('flash_message', 'Engine updated!');

        return redirect('engines?type_id='.$type_id.'&cat_id='.$cat_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, Request $request)
    {
        Engine::destroy($id);

        $type_id = $request->get('type_id'); 

        $cat_id = $request->get('cat_id'); 

        Session::flash('flash_message', 'Engine deleted!');

        return redirect('engines?type_id='.$type_id.'&cat_id='.$cat_id);
    }

    public function delete_item(Request $request){

        $item_id = $request->get('item_id'); 

        if($request->get('user_id')){
            $user_id = $request->get('user_id');
        }else{
            $user_id = 0;
        }

         
        if($item_id){


            $get_result = DB::table('engines')->where('id', $item_id)->update( ['deleted' => 1, 'delete_user_id'=>$user_id] );
            //echo $get_result;
            if($get_result == 1){
               echo 1; 
            }else{
                echo 1;
            }

        }else{
            echo 0;
        }

        

    }

}

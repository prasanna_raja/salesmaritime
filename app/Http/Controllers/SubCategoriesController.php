<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SubCategory;
use Illuminate\Http\Request;
use Session;
use DB;

class SubCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $subcategories = SubCategory::where('parent_id', 'LIKE', "%$keyword%")
				->orWhere('category_name', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $subcategories = SubCategory::paginate($perPage);
        }

        return view('sub-categories.index', compact('subcategories'));
    }

    public function view($id, Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $subcategories = SubCategory::where('parent_id', 'LIKE', "%$keyword%")
                ->orWhere('category_name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $subcategories = SubCategory::where('parent_id','=',$id)->paginate($perPage);
        }

        return view('sub-categories.index', ['subcategories'=>$subcategories, 'id'=>$id]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('sub-categories.create');
    }

    public function add($id)
    {
        $get_categories = DB::table('categories')->where('id', '=', $id)->get();

        foreach ($get_categories as $get_category) {
           $arr['category_name'] = $get_category->category_name;
           $arr['parent_id'] = $get_category->parent_id;
           $parent_id = $get_category->parent_id;
            $arr['parent_id'] = $get_category->id;

           $parents = DB::table('categories')->where('id', '=', $parent_id)->get();
           foreach ($parents as $parent) {
                $arr['type_name'] = $parent->category_name;
           }
        }

        return view('sub-categories.create', ['get_categories'=>$arr, 'id'=>$id]);
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        SubCategory::create($requestData);

        Session::flash('flash_message', 'SubCategory added!');

        return redirect('sub-categories');
    }

    public function insert($id, Request $request)
    {
        
        $requestData = $request->all();
        

        //print_r($requestData);exit;

        SubCategory::create($requestData);

        Session::flash('flash_message', 'SubCategory added!');

        return redirect('sub-categories/'.$id.'/view');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $subcategory = SubCategory::findOrFail($id);

        return view('sub-categories.show', compact('subcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($s_id, Request $request)
    {
        $req = $request->all();

        $id = $req['parent_id'];

        $get_categories = DB::table('categories')->where('id', '=', $id)->get();

        foreach ($get_categories as $get_category) {
           $arr['category_name'] = $get_category->category_name;
           $arr['parent_id'] = $get_category->parent_id;
           $parent_id = $get_category->parent_id;
            $arr['parent_id'] = $get_category->id;

           $parents = DB::table('categories')->where('id', '=', $parent_id)->get();
           foreach ($parents as $parent) {
                $arr['type_name'] = $parent->category_name;
           }
        }


        $subcategory = SubCategory::findOrFail($s_id);

        return view('sub-categories.edit', ['get_categories'=>$arr, 'subcategory'=>$subcategory, 'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $subcategory = SubCategory::findOrFail($id);
        $parents = DB::table('categories')->where('id', '=', $id)->get();
        foreach ($parents as $parent) {
            $parent_id = $parent->parent_id;
        }
        $subcategory->update($requestData);

        Session::flash('flash_message', 'SubCategory updated!');

        return redirect('sub-categories/'.$parent_id.'/view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $parents = DB::table('categories')->where('id', '=', $id)->get();
        foreach ($parents as $parent) {
            $parent_id = $parent->parent_id;
        }
        
        SubCategory::destroy($id);

        Session::flash('flash_message', 'SubCategory deleted!');

        return redirect('sub-categories/'.$parent_id.'/view');
    }


}

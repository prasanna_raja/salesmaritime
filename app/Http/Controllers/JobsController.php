<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Job;
use Illuminate\Http\Request;
use Session;
use Auth;
use DB;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jobs = Job::where('date_add', 'LIKE', "%$keyword%")
				->orWhere('user_id', 'LIKE', "%$keyword%")
				->orWhere('company', 'LIKE', "%$keyword%")
				->orWhere('title', 'LIKE', "%$keyword%")
				->orWhere('description', 'LIKE', "%$keyword%")
				->orWhere('job_type', 'LIKE', "%$keyword%")
				->orWhere('experience', 'LIKE', "%$keyword%")
				->orWhere('req_skill', 'LIKE', "%$keyword%")
				->orWhere('city', 'LIKE', "%$keyword%")
				->orWhere('country', 'LIKE', "%$keyword%")
				->orWhere('rem_currency', 'LIKE', "%$keyword%")
				->orWhere('rem_min', 'LIKE', "%$keyword%")
				->orWhere('rem_max', 'LIKE', "%$keyword%")
				->orWhere('close_date', 'LIKE', "%$keyword%")
				->orWhere('job_category_id', 'LIKE', "%$keyword%")
				->orWhere('jobsub_category_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $jobs = Job::paginate($perPage);
        }

        return view('jobs.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user_id = Auth::id();
        if($user_id != ''){
            $job_types = DB::table('job_types')->get();
            $job_experiences = DB::table('job_experiences')->get();
            $countries = DB::table('countries')->get();
            $job_categories = DB::table('job_categories')->where('parent_id', '=', 0)->get();
            $jobsub_categories = DB::table('job_categories')->where('parent_id', '!=', 0)->get();
            return view('jobs.create', ['user_id'=>$user_id, 'job_types'=>$job_types, 'job_experiences'=>$job_experiences, 'countries'=>$countries, 'job_categories'=>$job_categories, 'jobsub_categories'=>$jobsub_categories]);
        }else{
            return view('auth.login');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Job::create($requestData);

        Session::flash('flash_message', 'Job added!');

        return redirect('jobs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $job = Job::findOrFail($id);

        return view('jobs.show', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user_id = Auth::id();
        if($user_id != ''){
            
            $job_types = DB::table('job_types')->get();
            $job_experiences = DB::table('job_experiences')->get();
            $countries = DB::table('countries')->get();
            $job_categories = DB::table('job_categories')->where('parent_id', '=', 0)->get();
            $jobsub_categories = DB::table('job_categories')->where('parent_id', '!=', 0)->get();
            $job = Job::findOrFail($id);

            return view('jobs.edit', ['job'=>$job, 'user_id'=>$user_id, 'job_types'=>$job_types, 'job_experiences'=>$job_experiences, 'countries'=>$countries, 'job_categories'=>$job_categories, 'jobsub_categories'=>$jobsub_categories]);
        }else{
            return view('auth.login');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $job = Job::findOrFail($id);
        $job->update($requestData);

        Session::flash('flash_message', 'Job updated!');

        return redirect('jobs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Job::destroy($id);

        Session::flash('flash_message', 'Job deleted!');

        return redirect('jobs');
    }
}

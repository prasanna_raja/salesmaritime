<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Other_inq;
use Illuminate\Http\Request;
use Session;

class Other_inqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $other_inq = Other_inq::where('date_add', 'LIKE', "%$keyword%")
				->orWhere('date', 'LIKE', "%$keyword%")
				->orWhere('subject', 'LIKE', "%$keyword%")
				->orWhere('description', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->orWhere('user_id', 'LIKE', "%$keyword%")
				->orWhere('deleted', 'LIKE', "%$keyword%")
				->orWhere('delete_user_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $other_inq = Other_inq::paginate($perPage);
        }

        return view('other_inq.index', compact('other_inq'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('other_inq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Other_inq::create($requestData);

        Session::flash('flash_message', 'Other_inq added!');

        return redirect('other_inq');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $other_inq = Other_inq::findOrFail($id);

        return view('other_inq.show', compact('other_inq'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $other_inq = Other_inq::findOrFail($id);

        return view('other_inq.edit', compact('other_inq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $other_inq = Other_inq::findOrFail($id);
        $other_inq->update($requestData);

        Session::flash('flash_message', 'Other_inq updated!');

        return redirect('other_inq');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Other_inq::destroy($id);

        Session::flash('flash_message', 'Other_inq deleted!');

        return redirect('other_inq');
    }
}

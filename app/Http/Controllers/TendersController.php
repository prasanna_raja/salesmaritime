<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Tender;
use Illuminate\Http\Request;
use Session;

class TendersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tenders = Tender::where('user_id', 'LIKE', "%$keyword%")
				->orWhere('project_name', 'LIKE', "%$keyword%")
				->orWhere('company_name', 'LIKE', "%$keyword%")
				->orWhere('address', 'LIKE', "%$keyword%")
				->orWhere('city', 'LIKE', "%$keyword%")
				->orWhere('zip', 'LIKE', "%$keyword%")
				->orWhere('country', 'LIKE', "%$keyword%")
				->orWhere('phone', 'LIKE', "%$keyword%")
				->orWhere('fax', 'LIKE', "%$keyword%")
				->orWhere('email', 'LIKE', "%$keyword%")
				->orWhere('website', 'LIKE', "%$keyword%")
				->orWhere('contact_person', 'LIKE', "%$keyword%")
				->orWhere('description', 'LIKE', "%$keyword%")
				->orWhere('tender_cost', 'LIKE', "%$keyword%")
				->orWhere('date_invitation', 'LIKE', "%$keyword%")
				->orWhere('date_closing', 'LIKE', "%$keyword%")
				->orWhere('tender_status', 'LIKE', "%$keyword%")
				->orWhere('file', 'LIKE', "%$keyword%")
				->orWhere('deleted', 'LIKE', "%$keyword%")
				->orWhere('date_del', 'LIKE', "%$keyword%")
				->orWhere('delete_user_id', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $tenders = Tender::paginate($perPage);
        }

        return view('tenders.index', compact('tenders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('tenders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        

        

        if ($request->hasFile('file')) {
            $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->file->getClientOriginalExtension();
        
              $request->file->move(public_path('images/file'), $imageName);
                   
              $requestData['file'] = $imageName;
        }

        Tender::create($requestData);

        Session::flash('flash_message', 'Tender added!');

        return redirect('tenders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $tender = Tender::findOrFail($id);

        return view('tenders.show', compact('tender'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tender = Tender::findOrFail($id);

        return view('tenders.edit', compact('tender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

        

        if ($request->hasFile('file')) {
            $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->file->getClientOriginalExtension();
        
              $request->file->move(public_path('images/file'), $imageName);
                   
              $requestData['file'] = $imageName;
        }

        $tender = Tender::findOrFail($id);
        $tender->update($requestData);

        Session::flash('flash_message', 'Tender updated!');

        return redirect('tenders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Tender::destroy($id);

        Session::flash('flash_message', 'Tender deleted!');

        return redirect('tenders');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\user;
use Illuminate\Http\Request;
use Session;
use DB;
use Auth;
use Redirect;

class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $users = user::where('user_name', 'LIKE', "%$keyword%")
				->orWhere('email', 'LIKE', "%$keyword%")
				->orWhere('password', 'LIKE', "%$keyword%")
				->orWhere('gender', 'LIKE', "%$keyword%")
				->orWhere('group_id', 'LIKE', "%$keyword%")
				->orWhere('ip', 'LIKE', "%$keyword%")
				->orWhere('reg_date', 'LIKE', "%$keyword%")
				->orWhere('last_login_date', 'LIKE', "%$keyword%")
				->orWhere('expire_date', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->orWhere('first_name', 'LIKE', "%$keyword%")
				->orWhere('last_name', 'LIKE', "%$keyword%")
				->orWhere('company_name', 'LIKE', "%$keyword%")
				->orWhere('title_position', 'LIKE', "%$keyword%")
				->orWhere('country_id', 'LIKE', "%$keyword%")
				->orWhere('city', 'LIKE', "%$keyword%")
				->orWhere('address', 'LIKE', "%$keyword%")
				->orWhere('address2', 'LIKE', "%$keyword%")
				->orWhere('po_box', 'LIKE', "%$keyword%")
				->orWhere('tel', 'LIKE', "%$keyword%")
				->orWhere('fax', 'LIKE', "%$keyword%")
				->orWhere('mobile_number', 'LIKE', "%$keyword%")
				->orWhere('website', 'LIKE', "%$keyword%")
				->orWhere('newsletter', 'LIKE', "%$keyword%")
				->orWhere('remember_token', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $users = user::paginate($perPage);
        }

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user_roles = DB::table('user_roles')->pluck('role_name', 'id');
        $countries = DB::table('countries')->pluck('country_name', 'id');
        return view('users.create',['user_roles'=>$user_roles, 'countries'=>$countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        user::create($requestData);

        Session::flash('flash_message', 'user added!');

        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = user::findOrFail($id);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = user::findOrFail($id);

        $user_roles = DB::table('user_roles')->where('id', $id)->pluck('role_name', 'id');
        $countries = DB::table('countries')->pluck('country_name', 'id');


        return view('users.edit', compact('user', 'user_roles','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $user = user::findOrFail($id);
        $user->update($requestData);

        Session::flash('flash_message', 'user updated!');

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        user::destroy($id);

        Session::flash('flash_message', 'user deleted!');

        return redirect('users');
    }

    public function getProfile(){
      if (Auth::check())
      {
        $user_id  = Auth::user()->id;
        $users = DB::table('users')->where('id', $user_id)->get();
        
        $user_roles = DB::table('user_roles')->pluck('role_name', 'id');
        $countries = DB::table('countries')->get();

        return view('users.profile',['profile'=>$users, 'user_roles'=>$user_roles, 'countries'=>$countries]);
      }else{
        return Redirect::to('/');
      }
    }

    public function updateProfile(Request $request){
        $requestData = $request->all();

            $user_id = $requestData['user_id'];

            $get_users = DB::table('users')->where('id', $user_id)->get();

            foreach ($get_users as $get_user) {
                $g_first_name = $get_user->first_name;
                $g_last_name = $get_user->last_name;
                $g_user_name = $get_user->user_name;
                $g_email = $get_user->email;
                $g_password = $get_user->password;
                $g_company_name = $get_user->company_name;
                $g_title_position = $get_user->title_position;
                $g_country_id = $get_user->country_id;
                $g_city = $get_user->city;
                $g_address = $get_user->address;
                $g_address2 = $get_user->address2;
                $g_po_box = $get_user->po_box;
                $g_tel = $get_user->tel;
                $g_fax = $get_user->fax;
                $g_mobile_number = $get_user->mobile_number;
                $g_website = $get_user->website;
                $g_newsletter = $get_user->newsletter;
            }

            if($requestData['first_name'] == ''){
                $first_name = $requestData['first_name'];
            }else{
                $first_name = $g_first_name;
            }

            if($requestData['last_name'] == ''){
                $last_name = $requestData['last_name'];
            }else{
                $last_name = $g_last_name;
            }

            if($requestData['user_name'] == ''){
                $user_name = $requestData['user_name'];
            }else{
                $user_name = $g_user_name;
            }

            if($requestData['email'] == ''){
                $email = $requestData['email'];
            }else{
                $email = $g_email;
            }

            if($requestData['password'] == ''){
                $password = $requestData['password'];
            }else{
                $password = $g_password;
            }

            if($requestData['company_name'] == ''){
                $company_name = $requestData['company_name'];
            }else{
                $company_name = $g_company_name;
            }

            if($requestData['title_position'] == ''){
                $title_position = $requestData['title_position'];
            }else{
                $title_position = $g_title_position;
            }

            if($requestData['country_id'] == ''){
                $country_id = $requestData['country_id'];
            }else{
                $country_id = $g_country_id;
            }

            if($requestData['city'] == ''){
                $city = $requestData['city'];
            }else{
                $city = $g_city;
            }

            if($requestData['address'] == ''){
                $address = $requestData['address'];
            }else{
                $address = $g_address;
            }

            if($requestData['address2'] == ''){
                $address2 = $requestData['address2'];
            }else{
                $address2 = $g_address2;
            }

            if($requestData['po_box'] == ''){
                $po_box = $requestData['po_box'];
            }else{
                $po_box = $g_po_box;
            }

            if($requestData['tel'] == ''){
                $tel = $requestData['tel'];
            }else{
                $tel = $g_tel;
            }

            if($requestData['fax'] == ''){
                $fax = $requestData['fax'];
            }else{
                $fax = $g_fax;
            }

            if($requestData['mobile_number'] == ''){
                $mobile_number = $requestData['mobile_number'];
            }else{
                $mobile_number = $g_mobile_number;
            }

            if($requestData['website'] == ''){
                $website = $requestData['website'];
            }else{
                $website = $g_website;
            }

            if($requestData['newsletter'] == ''){
                $newsletter = $requestData['newsletter'];
            }else{
                $newsletter = $g_newsletter;
            }
print_r($requestData);//exit;
      

            $update_result = DB::table('users')->where('id','=', $user_id)->update([
                'first_name'=>$first_name, 
                'last_name'=>$last_name, 
                'user_name'=>$user_name, 
                'email'=>$email, 
                'company_name'=>$company_name, 
                'title_position'=>$title_position, 
                'country_id'=>$country_id, 
                'city'=>$city, 
                'address'=>$address, 
                'address2'=>$address2, 
                'po_box'=>$po_box, 
                'tel'=>$tel, 
                'fax'=>$fax, 
                'mobile_number'=>$mobile_number, 
                'website'=>$website, 
                'newsletter'=>$newsletter
                ]);
            
            echo $update_result;

        
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\cv_launguage;
use Illuminate\Http\Request;
use Session;

class cv_launguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cv_launguage = cv_launguage::where('cv_id', 'LIKE', "%$keyword%")
				->orWhere('lang', 'LIKE', "%$keyword%")
				->orWhere('level', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $cv_launguage = cv_launguage::paginate($perPage);
        }

        return view('cv_launguage.index', compact('cv_launguage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cv_launguage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        cv_launguage::create($requestData);

        Session::flash('flash_message', 'cv_launguage added!');

        return redirect('cv_launguage');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $cv_launguage = cv_launguage::findOrFail($id);

        return view('cv_launguage.show', compact('cv_launguage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cv_launguage = cv_launguage::findOrFail($id);

        return view('cv_launguage.edit', compact('cv_launguage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $cv_launguage = cv_launguage::findOrFail($id);
        $cv_launguage->update($requestData);

        Session::flash('flash_message', 'cv_launguage updated!');

        return redirect('cv_launguage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        cv_launguage::destroy($id);

        Session::flash('flash_message', 'cv_launguage deleted!');

        return redirect('cv_launguage');
    }
}

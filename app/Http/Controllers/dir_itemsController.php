<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\dir_item;
use Illuminate\Http\Request;
use Session;
use DB;
use Auth;

class dir_itemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (Auth::check()){
             
            $user_id = Auth::id();

            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $dir_items = dir_item::where('user_id', '=', $user_id)->paginate($perPage);
            } else {
                $dir_items = dir_item::where('user_id', '=', $user_id)->paginate($perPage);
            }

            $already_dir = 0;
            foreach($dir_items as $dir_item){
                $item_id = $dir_item->id;
                if($dir_item->user_id){
                    $already_dir = 1;
                }else{
                    $already_dir = 0;
                }
            }

            echo $already_dir;

            $dir_categories = DB::table('dir_categories')->where('parent_id', '=', 0)->get();
            $dir_subcategories = DB::table('dir_categories')->where('parent_id', '!=', 0)->get();
            
            $users = DB::table('users')->get();

            return view('dir_items.index', compact('dir_items', 'dir_categories', 'dir_subcategories', 'users', 'user_id', 'already_dir', 'item_id'));

        }else{

            return view('auth.login');

        }


        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        if (Auth::check()){

            $dir_categories = DB::table('dir_categories')->where('parent_id', '=', 0)->get();
            $dir_subcategories = DB::table('dir_categories')->where('parent_id', '!=', 0)->get();

            $user_id = Auth::id();
            $users = DB::table('users')->where('id', '=', $user_id)->get();

            return view('dir_items.create', ['dir_categories'=>$dir_categories, 'dir_subcategories'=>$dir_subcategories, 'users'=>$users]);

        }else{

           return view('auth.login'); 

        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

       // print_r($requestData);exit;
        
        dir_item::create($requestData);

        Session::flash('flash_message', 'dir_item added!');

        return redirect('dir_items');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        if (Auth::check()){

            $dir_item = dir_item::findOrFail($id);

            return view('dir_items.show', compact('dir_item'));

        }else{

           return view('auth.login'); 
           
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (Auth::check()){

            $dir_categories = DB::table('dir_categories')->where('parent_id', '=', 0)->get();
            $dir_subcategories = DB::table('dir_categories')->where('parent_id', '!=', 0)->get();

            $user_id = Auth::id();
            $users = DB::table('users')->where('id', '=', $user_id)->get();

            $dir_item = dir_item::findOrFail($id);

            return view('dir_items.edit', compact('dir_item', 'dir_categories', 'dir_subcategories', 'users'));

        }else{

           return view('auth.login'); 
           
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $dir_item = dir_item::findOrFail($id);
        $dir_item->update($requestData);

        Session::flash('flash_message', 'dir_item updated!');

        return redirect('dir_items');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if (Auth::check()){

            dir_item::destroy($id);

            Session::flash('flash_message', 'dir_item deleted!');

            return redirect('dir_items');

        }else{

           return view('auth.login'); 
           
        }
        
    }
}

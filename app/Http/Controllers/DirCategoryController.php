<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DirCategory;
use Illuminate\Http\Request;
use Session;

class DirCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $dircategory = DirCategory::where('parent_id', 'LIKE', "%$keyword%")
				->orWhere('dir_category_name', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $dircategory = DirCategory::where('parent_id', '=', 0)->paginate($perPage);
        }

        return view('dir-category.index', compact('dircategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('dir-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        DirCategory::create($requestData);

        Session::flash('flash_message', 'DirCategory added!');

        return redirect('dir-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $dircategory = DirCategory::findOrFail($id);

        return view('dir-category.show', compact('dircategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $dircategory = DirCategory::findOrFail($id);

        return view('dir-category.edit', compact('dircategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $dircategory = DirCategory::findOrFail($id);
        $dircategory->update($requestData);

        Session::flash('flash_message', 'DirCategory updated!');

        return redirect('dir-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DirCategory::destroy($id);

        Session::flash('flash_message', 'DirCategory deleted!');

        return redirect('dir-category');
    }
}

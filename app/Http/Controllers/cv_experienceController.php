<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\cv_experience;
use Illuminate\Http\Request;
use Session;

class cv_experienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cv_experience = cv_experience::where('cv_id', 'LIKE', "%$keyword%")
				->orWhere('company', 'LIKE', "%$keyword%")
				->orWhere('position', 'LIKE', "%$keyword%")
				->orWhere('responsibility', 'LIKE', "%$keyword%")
				->orWhere('start_work', 'LIKE', "%$keyword%")
				->orWhere('end_work', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $cv_experience = cv_experience::paginate($perPage);
        }

        return view('cv_experience.index', compact('cv_experience'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cv_experience.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        cv_experience::create($requestData);

        Session::flash('flash_message', 'cv_experience added!');

        return redirect('cv_experience');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $cv_experience = cv_experience::findOrFail($id);

        return view('cv_experience.show', compact('cv_experience'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cv_experience = cv_experience::findOrFail($id);

        return view('cv_experience.edit', compact('cv_experience'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $cv_experience = cv_experience::findOrFail($id);
        $cv_experience->update($requestData);

        Session::flash('flash_message', 'cv_experience updated!');

        return redirect('cv_experience');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        cv_experience::destroy($id);

        Session::flash('flash_message', 'cv_experience deleted!');

        return redirect('cv_experience');
    }
}

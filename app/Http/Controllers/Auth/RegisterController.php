<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'user_name' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'gender' => 'required|string|max:255',
            'company_name' => 'required|string|max:255',
            'title_position' => 'required|string|max:255',
            'country_id' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'address2' => 'required|string|max:255',
            'po_box' => 'required|string|max:255',
            'tel' => 'required|string|max:255',
            'fax' => 'required|string|max:255',
            'mobile_number' => 'required|string|max:255',
            'website' => 'required|string|max:255',
            'newsletter' => 'required|string|max:255',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([

            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'user_name' => $data['user_name'],
            'password' => bcrypt($data['password']),
            'gender' => $data['gender'],
            'company_name' => $data['company_name'],
            'title_position' => $data['title_position'],
            'country_id' => $data['country_id'],
            'city' => $data['city'],
            'address' => $data['address'],
            'address2' => $data['address2'],
            'po_box' => $data['po_box'],
            'tel' => $data['tel'],
            'fax' => $data['fax'],
            'mobile_number' => $data['mobile_number'],
            'website' => $data['website'],
            'newsletter' => $data['newsletter'],

        ]);
    }

    public function showRegistrationForm() {
        
        $countries = DB::table('countries')->get();
        return view ('auth.register', ['countries'=>$countries]);
    }
}

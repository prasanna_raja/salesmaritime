<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\user_role;
use Illuminate\Http\Request;
use Session;

class user_rolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $user_roles = user_role::where('role_name', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $user_roles = user_role::paginate($perPage);
        }

        return view('user_roles.index', compact('user_roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('user_roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        user_role::create($requestData);

        Session::flash('flash_message', 'user_role added!');

        return redirect('user_roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user_role = user_role::findOrFail($id);

        return view('user_roles.show', compact('user_role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user_role = user_role::findOrFail($id);

        return view('user_roles.edit', compact('user_role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $user_role = user_role::findOrFail($id);
        $user_role->update($requestData);

        Session::flash('flash_message', 'user_role updated!');

        return redirect('user_roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        user_role::destroy($id);

        Session::flash('flash_message', 'user_role deleted!');

        return redirect('user_roles');
    }
}

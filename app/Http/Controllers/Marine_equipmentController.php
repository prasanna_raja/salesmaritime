<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Marine_equipment;
use Illuminate\Http\Request;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;

class Marine_equipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user_id = Auth::id();
        if($user_id != ''){

            $keyword = $request->get('search');
            $type_id = $request->get('type_id');
            $cat_id = $request->get('cat_id');
            
            $perPage = 25;

            if (!empty($keyword)) {
                $marine_equipment = Marine_equipment::where('category_id', 'LIKE', "%$keyword%")
    				->orWhere('sub_category_id', 'LIKE', "%$keyword%")
    				->orWhere('photo', 'LIKE', "%$keyword%")
    				->orWhere('protocol_file', 'LIKE', "%$keyword%")
    				->orWhere('user_id', 'LIKE', "%$keyword%")
    				->orWhere('description', 'LIKE', "%$keyword%")
    				->orWhere('date_add', 'LIKE', "%$keyword%")
    				->orWhere('date_del', 'LIKE', "%$keyword%")
    				->orWhere('status', 'LIKE', "%$keyword%")
    				->orWhere('purchase', 'LIKE', "%$keyword%")
    				->orWhere('sold', 'LIKE', "%$keyword%")
    				->orWhere('year_of_build', 'LIKE', "%$keyword%")
    				->orWhere('country_build', 'LIKE', "%$keyword%")
    				->orWhere('name', 'LIKE', "%$keyword%")
    				->orWhere('make', 'LIKE', "%$keyword%")
    				->orWhere('model', 'LIKE', "%$keyword%")
    				->orWhere('condition', 'LIKE', "%$keyword%")
    				->orWhere('location', 'LIKE', "%$keyword%")
    				->orWhere('price', 'LIKE', "%$keyword%")
    				->orWhere('deleted', 'LIKE', "%$keyword%")
    				->orWhere('delete_user_id', 'LIKE', "%$keyword%")
    				->paginate($perPage);
            } else {
                $marine_equipment = Marine_equipment::where([ ['type_id', '=', $type_id], ['category_id', '=', $cat_id] ])->leftjoin('users','marine_equipments.user_id', '=', 'users.id')
                ->select('marine_equipments.id', 'marine_equipments.date_add', 'marine_equipments.deleted', 'marine_equipments.photo', 'marine_equipments.year_of_build', 'marine_equipments.location', 'marine_equipments.description', 'marine_equipments.price', 'marine_equipments.user_id', 'marine_equipments.status', 'marine_equipments.sold', 'users.first_name', 'users.last_name')
                ->paginate($perPage);
               // $marine_equipment = Marine_equipment::paginate($perPage);
            }

            return view('marine_equipment.index', ['marine_equipment'=>$marine_equipment, 'type_id'=>$type_id, 'cat_id'=>$cat_id, 'user_id'=>$user_id]);
        }else{
            return view('auth.login');
        }    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        
        $user_id = Auth::id();
        if($user_id != ''){

            $type_id = $request->get('type_id');
            $cat_id = $request->get('cat_id');
            $categories = DB::table('categories')->where([ ['type', '=', 1],['parent_id', '=', $type_id] ])->get();

            $sub_categories = DB::table('categories')->where([ ['type', '=', 2], ['parent_id', '=', $cat_id] ])->get();
            $countries = DB::table('countries')->get();
            
            return view('marine_equipment.create', ['categories'=>$categories, 'sub_categories'=>$sub_categories, 'user_id' => $user_id, 'countries'=>$countries, 'type_id'=>$type_id, 'cat_id'=>$cat_id]);
        }else{
            return view('auth.login');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $type_id = $request->get('type_id'); 

        $cat_id = $request->get('category_id'); 

        if ($request->hasFile('photo')) {
            $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        
              $request->photo->move(public_path('images/photo'), $imageName);
                   
              $requestData['photo'] = $imageName;
        }


        if ($request->hasFile('protocol_file')) {
            $this->validate($request, [
            'protocol_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName1 = time().'.'.$request->protocol_file->getClientOriginalExtension();
        
              $request->protocol_file->move(public_path('images/protocol_file'), $imageName1);
                   
              $requestData['protocol_file'] = $imageName1;
        }

        Marine_equipment::create($requestData);

        Session::flash('flash_message', 'Marine_equipment added!');

        return redirect('marine_equipment?type_id='.$type_id.'&cat_id='.$cat_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $marine_equipment = Marine_equipment::findOrFail($id);

        $type_id = $request->get('type_id');
        $cat_id = $request->get('cat_id');



        return view('marine_equipment.show', compact('marine_equipment', 'type_id', 'cat_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $marine_equipment = Marine_equipment::findOrFail($id);

        $type_id = $request->get('type_id'); 

        $cat_id = $request->get('cat_id'); 

        $categories = DB::table('categories')->where([ ['type', '=', 1],['parent_id', '=', $type_id] ])->get();

        $sub_categories = DB::table('categories')->where([ ['type', '=', 2], ['parent_id', '=', $cat_id] ])->get();
        $countries = DB::table('countries')->get();

        return view('marine_equipment.edit', ['marine_equipment'=>$marine_equipment, 'categories'=>$categories, 'sub_categories'=>$sub_categories, 'user_id'=>$id, 'countries'=>$countries, 'type_id'=>$type_id, 'cat_id'=>$cat_id]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

        if ($request->hasFile('photo')) {
            $this->validate($request, [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        
              $request->photo->move(public_path('images/photo'), $imageName);
                   
              $requestData['photo'] = $imageName;
        }


        if ($request->hasFile('protocol_file')) {
            $this->validate($request, [
            'protocol_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
              $imageName1 = time().'.'.$request->protocol_file->getClientOriginalExtension();
        
              $request->protocol_file->move(public_path('images/protocol_file'), $imageName1);
                   
              $requestData['protocol_file'] = $imageName1;
        }

        $marine_equipment = Marine_equipment::findOrFail($id);
        $marine_equipment->update($requestData);

        $type_id = $requestData['type_id'];

        $cat_id = $requestData['category_id'];

        Session::flash('flash_message', 'Marine_equipment updated!');

        return redirect('marine_equipment?type_id='.$type_id.'&cat_id='.$cat_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, Request $request)
    {
        Marine_equipment::destroy($id);

        $type_id = $request->get('type_id'); 

        $cat_id = $request->get('cat_id'); 

        Session::flash('flash_message', 'Marine_equipment deleted!');

        return redirect('marine_equipment?type_id='.$type_id.'&cat_id='.$cat_id);
    }

    public function delete_item(Request $request){

        $item_id = $request->get('item_id'); 

        if($request->get('user_id')){
            $user_id = $request->get('user_id');
        }else{
            $user_id = 0;
        }

         
        if($item_id){


            $get_result = DB::table('marine_equipments')->where('id', $item_id)->update( ['deleted' => 1, 'delete_user_id'=>$user_id] );
            //echo $get_result;
            if($get_result == 1){
               echo 1; 
            }else{
                echo 1;
            }

        }else{
            echo 0;
        }

        

    }

}

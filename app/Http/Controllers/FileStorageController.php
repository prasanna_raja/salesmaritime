<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\FileStorage;
use Illuminate\Http\Request;
use Session;

class FileStorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $filestorage = FileStorage::where('user_id', 'LIKE', "%$keyword%")
				->orWhere('file', 'LIKE', "%$keyword%")
				->orWhere('description', 'LIKE', "%$keyword%")
				->orWhere('date_add', 'LIKE', "%$keyword%")
				->orWhere('date_del', 'LIKE', "%$keyword%")
				->orWhere('download', 'LIKE', "%$keyword%")
				->orWhere('delete_user_id', 'LIKE', "%$keyword%")
				->orWhere('deleted', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $filestorage = FileStorage::paginate($perPage);
        }

        return view('file-storage.index', compact('filestorage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('file-storage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        

        if ($request->hasFile('file')) {
            foreach($request['file'] as $file){
                $uploadPath = public_path('/uploads/file');

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['file'] = $fileName;
            }
        }

        FileStorage::create($requestData);

        Session::flash('flash_message', 'FileStorage added!');

        return redirect('file-storage');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $filestorage = FileStorage::findOrFail($id);

        return view('file-storage.show', compact('filestorage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $filestorage = FileStorage::findOrFail($id);

        return view('file-storage.edit', compact('filestorage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

        if ($request->hasFile('file')) {
            foreach($request['file'] as $file){
                $uploadPath = public_path('/uploads/file');

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['file'] = $fileName;
            }
        }

        $filestorage = FileStorage::findOrFail($id);
        $filestorage->update($requestData);

        Session::flash('flash_message', 'FileStorage updated!');

        return redirect('file-storage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        FileStorage::destroy($id);

        Session::flash('flash_message', 'FileStorage deleted!');

        return redirect('file-storage');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\JobCategory;
use Illuminate\Http\Request;
use Session;
use DB;

class JobsubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jobsubcategory = JobCategory::where('parent_id', 'LIKE', "%$keyword%")
				->orWhere('jobcategory_name', 'LIKE', "%$keyword%")
				->orWhere('status', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $jobsubcategory = JobCategory::where('parent_id', '!=', 0)
                ->paginate($perPage);
        }

        return view('jobsub-category.index', compact('jobsubcategory'));
    }

    public function view($id, Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jobsubcategory = JobCategory::where('parent_id', 'LIKE', "%$keyword%")
                ->orWhere('jobcategory_name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $jobsubcategory = JobCategory::where('parent_id','=',$id)->paginate($perPage);
        }

        return view('jobsub-category.index', ['jobsubcategory'=>$jobsubcategory, 'id'=>$id]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('jobsub-category.create');
    }

    public function add($id)
    {
        $get_categories = DB::table('job_categories')->where('id', '=', $id)->get();

        foreach ($get_categories as $get_category) {
            
            $arr['category_name'] = $get_category->jobcategory_name;
            $arr['parent_id'] = $get_category->parent_id;
            $arr['parent_id'] = $get_category->id;

           
        }

        return view('jobsub-category.create', ['get_categories'=>$arr, 'id'=>$id]);
    }  

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        JobCategory::create($requestData);

        Session::flash('flash_message', 'JobsubCategory added!');

        return redirect('jobsub-category');
    }


    public function insert($id, Request $request)
    {
        
        $requestData = $request->all();
        

        //print_r($requestData);exit;

        JobCategory::create($requestData);

        Session::flash('flash_message', 'SubCategory added!');

        return redirect('jobsub-category/'.$id.'/view');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $jobsubcategory = JobCategory::findOrFail($id);

        return view('jobsub-category.show', compact('jobsubcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($s_id, Request $request)
    {
        $req = $request->all();

        $id = $req['parent_id'];

        $get_categories = DB::table('job_categories')->where('id', '=', $id)->get();

        foreach ($get_categories as $get_category) {
           $arr['category_name'] = $get_category->jobcategory_name;
           $arr['parent_id'] = $get_category->parent_id;
            $arr['parent_id'] = $get_category->id;

           
        }


        $jobsubcategory = JobCategory::findOrFail($s_id);

        return view('jobsub-category.edit', ['get_categories'=>$arr, 'jobsubcategory'=>$jobsubcategory, 'id'=>$id]);

        /*$jobsubcategory = JobCategory::findOrFail($id);

        return view('jobsub-category.edit', compact('jobsubcategory'));*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        

        $requestData = $request->all();
        
        $jobsubcategory = JobCategory::findOrFail($id);

        $parents = DB::table('job_categories')->where('id', '=', $id)->get();
        foreach ($parents as $parent) {
            $parent_id = $parent->parent_id;
        }

        $jobsubcategory->update($requestData);

        Session::flash('flash_message', 'JobsubCategory updated!');

        return redirect('jobsub-category/'.$parent_id.'/view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
       

        $parents = DB::table('job_categories')->where('id', '=', $id)->get();
        foreach ($parents as $parent) {
            $parent_id = $parent->parent_id;
        }

        JobCategory::destroy($id);

        Session::flash('flash_message', 'JobsubCategory deleted!');

        return redirect('jobsub-category/'.$parent_id.'/view');
    }
}

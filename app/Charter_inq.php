<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charter_inq extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'charters';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'sub_category_id', 'photo', 'protocol_file', 'user_id', 'description', 'date_add', 'date_del', 'date_start', 'date_end', 'status', 'purchase', 'sold', 'year_of_build', 'country_build', 'name', 'condition', 'location', 'class', 'imo_number', 'crews', 'propellers', 'knote', 'location_from', 'location_to', 'hp', 'measurement', 'length', 'breadth', 'depth', 'tonnage_net', 'tonnage_gross', 'light_weight', 'dead_weight', 'price', 'deleted', 'delete_user_id', 'type_id'];

    
}

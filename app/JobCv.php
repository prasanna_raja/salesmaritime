<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCv extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_cvs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'job_category_id', 'jobsub_category_id', 'title', 'first_name', 'last_name', 'gender', 'date_add', 'date_expire', 'date_birth', 'need_salary', 'email', 'mobile', 'country', 'nationality', 'city', 'reloaction', 'lang_nat', 'cv_file', 'photo', 'deleted', 'date_del', 'delete_user_id', 'status'];

    
}

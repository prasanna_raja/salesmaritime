<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dir_item extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dir_items';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date_add', 'keywords', 'description', 'dir_category_id', 'dir_subcategory_id', 'user_id', 'status'];

    
}

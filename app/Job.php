<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date_add', 'user_id', 'company', 'title', 'description', 'job_type', 'experience', 'req_skill', 'city', 'country', 'rem_currency', 'rem_min', 'rem_max', 'close_date', 'job_category_id', 'jobsub_category_id'];

    
}

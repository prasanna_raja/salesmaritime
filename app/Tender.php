<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tender extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tenders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'project_name', 'company_name', 'address', 'city', 'zip', 'country', 'phone', 'fax', 'email', 'website', 'contact_person', 'description', 'tender_cost', 'date_invitation', 'date_closing', 'tender_status', 'file', 'deleted', 'date_del', 'delete_user_id', 'status'];

    
}

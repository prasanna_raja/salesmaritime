<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crane extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cranes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'sub_category_id', 'photo', 'protocol_file', 'user_id', 'description', 'date_add', 'date_del', 'status', 'purchase', 'sold', 'country_build', 'year_of_build', 'name', 'make', 'model', 'tonn', 'working_hours', 'condition', 'location', 'price', 'deleted', 'delete_user_id', 'type_id'];

    
}

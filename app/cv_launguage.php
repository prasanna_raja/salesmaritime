<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cv_launguage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_launguages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['cv_id', 'lang', 'level'];

    
}

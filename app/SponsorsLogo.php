<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SponsorsLogo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sponsors_logos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order', 'photo', 'user_id', 'description', 'date_add', 'date_start', 'date_expire', 'status', 'url', 'title'];

    
}

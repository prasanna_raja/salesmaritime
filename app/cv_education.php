<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cv_education extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cv_educations';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['cv_id', 'education_id', 'name', 'department', 'dip_deg', 'year_end'];

    

    
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullableCranesTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cranes', function (Blueprint $table) {
            $table->string('photo')->nulllable()->change();
            $table->string('protocol_file')->nulllable()->change();
            $table->dateTime('date_del')->nulllable()->change();
            $table->string('location')->nulllable()->change();
            $table->integer('price')->nulllable()->change();
            $table->integer('deleted')->nulllable()->change();
            $table->integer('delete_user_id')->nulllable()->change();
            $table->string('working_hours')->nulllable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cranes', function (Blueprint $table) {
            //
        });
    }
}

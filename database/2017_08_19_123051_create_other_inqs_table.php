<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOtherInqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_inqs', function(Blueprint $table) {
            $table->increments('id');
            $table->date('date_add');
            $table->string('date');
            $table->string('subject');
            $table->text('description');
            $table->integer('status');
            $table->integer('user_id');
            $table->integer('deleted');
            $table->integer('delete_user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('other_inqs');
    }
}

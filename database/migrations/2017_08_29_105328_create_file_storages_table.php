<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFileStoragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_storages', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('file')->nullable();
            $table->text('description')->nullable();
            $table->date('date_add')->nullable();
            $table->date('date_del')->nullable();
            $table->integer('download')->nullable();
            $table->integer('delete_user_id')->nullable();
            $table->integer('deleted')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('file_storages');
    }
}

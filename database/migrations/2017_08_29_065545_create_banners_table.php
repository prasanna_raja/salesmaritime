<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->nulllable();
            $table->string('site_section')->nulllable();
            $table->string('photo')->nulllable();
            $table->integer('image_width')->nulllable();
            $table->integer('image_height')->nulllable();
            $table->integer('user_id')->nulllable();
            $table->text('description')->nulllable();
            $table->date('date_add')->nulllable();
            $table->date('date_start')->nulllable();
            $table->date('date_expire')->nulllable();
            $table->integer('status')->nulllable()->default(1);
            $table->string('url')->nulllable();
            $table->string('title')->nulllable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banners');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDirItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dir_items', function(Blueprint $table) {
            $table->increments('id');
            $table->date('date_add');
            $table->string('keywords');
            $table->text('description');
            $table->integer('dir_category_id');
            $table->integer('dir_subcategory_id');
            $table->integer('user_id');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dir_items');
    }
}

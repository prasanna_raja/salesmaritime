<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DefaultValueSetYachts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yachts', function (Blueprint $table) {
            $table->increments('id')->change();
            $table->integer('category_id')->change();
            $table->integer('sub_category_id')->change();
            $table->string('photo')->nullable()->change();
            $table->string('protocol_file')->nullable()->change();
            $table->integer('user_id')->change();
            $table->string('description')->change();
            $table->dateTime('date_add')->change();
            $table->dateTime('date_del')->nullable()->change();
            $table->integer('status')->nullable()->default(1)->change();
            $table->integer('purchase')->nullable()->default(0)->change();
            $table->integer('sold')->nullable()->default(0)->change();
            $table->integer('year_of_build')->change();
            $table->integer('country_build')->change();
            $table->string('name')->nullable()->change();
            $table->integer('condition')->change();
            $table->string('location')->nullable()->change();
            $table->string('class')->nullable()->change();
            $table->string('imo_number')->nullable()->change();
            $table->string('hp')->nullable()->change();
            $table->string('measurement')->nullable()->change();
            $table->string('length')->nullable()->change();
            $table->string('breadth')->nullable()->change();
            $table->string('depth')->nullable()->change();
            $table->integer('price')->nullable()->change();
            $table->integer('deleted')->nullable()->change();
            $table->integer('delete_user_id')->nullable()->change();
        });
    }d

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yachts', function (Blueprint $table) {
            //
        });
    }
}

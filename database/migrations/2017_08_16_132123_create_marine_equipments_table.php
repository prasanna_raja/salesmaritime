<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMarineEquipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marine_equipments', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->string('photo')->nullable();
            $table->string('protocol_file')->nullable();
            $table->integer('user_id');
            $table->string('description');
            $table->dateTime('date_add');
            $table->dateTime('date_del')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->integer('purchase')->nullable()->default(0);
            $table->integer('sold')->nullable()->default(0);
            $table->string('year_of_build');
            $table->integer('country_build');
            $table->string('name');
            $table->string('make');
            $table->string('model');
            $table->string('condition');
            $table->string('location')->nullable();
            $table->integer('price')->nullable();
            $table->integer('deleted')->nullable();
            $table->integer('delete_user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('marine_equipments');
    }
}

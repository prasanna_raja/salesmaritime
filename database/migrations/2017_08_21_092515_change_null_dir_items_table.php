<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullDirItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dir_items', function (Blueprint $table) {
            $table->date('date_add')->nullable()->change();
            $table->string('keywords')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->integer('dir_category_id')->nullable()->change();
            $table->integer('dir_subcategory_id')->nullable()->change();
            $table->integer('user_id')->nullable()->change();
            $table->integer('status')->nullable()->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dir_items', function (Blueprint $table) {
            //
        });
    }
}

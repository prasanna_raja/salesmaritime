<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_name')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('gender');
            $table->integer('group_id');
            $table->string('ip');
            $table->dateTime('reg_date');
            $table->dateTime('last_login_date');
            $table->dateTime('expire_date');
            $table->integer('status');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('company_name');
            $table->string('title_position');
            $table->integer('country_id');
            $table->string('city');
            $table->string('address');
            $table->string('address2');
            $table->string('po_box');
            $table->string('tel');
            $table->string('fax');
            $table->string('mobile_number');
            $table->string('website');
            $table->integer('newsletter');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

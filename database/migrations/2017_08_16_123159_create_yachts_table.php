<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateYachtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yachts', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->string('photo');
            $table->string('protocol_file');
            $table->integer('user_id');
            $table->string('description');
            $table->dateTime('date_add');
            $table->dateTime('date_del');
            $table->integer('status');
            $table->integer('purchase');
            $table->integer('sold');
            $table->integer('year_of_build');
            $table->integer('country_build');
            $table->string('name');
            $table->integer('condition');
            $table->string('location');
            $table->string('class');
            $table->string('imo_number');
            $table->string('hp');
            $table->string('measurement');
            $table->string('length');
            $table->string('breadth');
            $table->string('depth');
            $table->integer('price');
            $table->integer('deleted');
            $table->integer('delete_user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('yachts');
    }
}

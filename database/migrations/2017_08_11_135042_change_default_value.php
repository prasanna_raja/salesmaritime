<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('ip')->nullable()->change();
            $table->datetime('reg_date')->nullable()->change();
            $table->datetime('last_login_date')->nullable()->change();
            $table->datetime('expire_date')->nullable()->change();
            $table->integer('status')->default('1')->change();
            $table->string('company_name')->nullable()->change();
            $table->string('title_position')->nullable()->change();
            $table->integer('country_id')->nullable()->change();
            $table->string('city')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('address2')->nullable()->change();
            $table->string('po_box')->nullable()->change();
            $table->string('tel')->nullable()->change();
            $table->string('fax')->nullable()->change();
            $table->string('mobile_number')->nullable()->change();
            $table->string('website')->nullable()->change();
            $table->integer('newsletter')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}

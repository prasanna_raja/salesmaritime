<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCvEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cv_educations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('cv_id')->nullable();
            $table->integer('education_id')->nullable();
            $table->string('name')->nullable();
            $table->string('department')->nullable();
            $table->string('dip_deg')->nullable();
            $table->string('year_end')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cv_educations');
    }
}

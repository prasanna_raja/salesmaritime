<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChartersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charters', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->string('photo')->nullable();
            $table->string('protocol_file')->nullable();
            $table->integer('user_id');
            $table->text('description');
            $table->date('date_add')->nullable();
            $table->date('date_del')->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->integer('purchase')->nullable()->default(0);
            $table->integer('sold')->nullable()->default(0);
            $table->integer('year_of_build')->nullable();
            $table->integer('country_build')->nullable();
            $table->string('name')->nullable();
            $table->integer('condition')->nullable();
            $table->string('location')->nullable();
            $table->string('class')->nullable();
            $table->string('imo_number')->nullable();
            $table->string('crews')->nullable();
            $table->string('propellers')->nullable();
            $table->string('knote')->nullable();
            $table->string('location_from')->nullable();
            $table->string('location_to')->nullable();
            $table->string('hp')->nullable();
            $table->string('measurement')->nullable();
            $table->string('length')->nullable();
            $table->string('breadth')->nullable();
            $table->string('depth')->nullable();
            $table->string('tonnage_net')->nullable();
            $table->string('tonnage_gross')->nullable();
            $table->string('light_weight')->nullable();
            $table->string('dead_weight')->nullable();
            $table->integer('price')->nullable();
            $table->integer('deleted')->nullable();
            $table->integer('delete_user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('charters');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function(Blueprint $table) {
            $table->increments('id');
            $table->date('date_add')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('company')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->integer('job_type')->nullable();
            $table->integer('experience')->nullable();
            $table->string('req_skill')->nullable();
            $table->string('city')->nullable();
            $table->integer('country')->nullable();
            $table->integer('rem_currency')->nullable();
            $table->integer('rem_min')->nullable();
            $table->integer('rem_max')->nullable();
            $table->date('close_date')->nullable();
            $table->integer('job_category_id')->nullable();
            $table->integer('jobsub_category_id')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobs');
    }
}

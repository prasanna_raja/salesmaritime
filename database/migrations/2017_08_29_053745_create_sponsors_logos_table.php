<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSponsorsLogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsors_logos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->nullable();
            $table->string('photo')->nullable();
            $table->integer('user_id')->nullable();
            $table->text('description')->nullable();
            $table->date('date_add')->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_expire')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->string('url')->nullable();
            $table->string('title')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sponsors_logos');
    }
}

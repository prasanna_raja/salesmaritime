<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVesselsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vessels', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->string('photo')->nullable();
            $table->string('protocol_file')->nullable();
            $table->integer('user_id');
            $table->text('description');
            $table->dateTime('date_add');
            $table->dateTime('date_del')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->integer('purchase')->nullable()->default(0);
            $table->integer('sold')->nullable()->default(0);
            $table->integer('year_of_build');
            $table->integer('country_build');
            $table->string('name')->nullable();
            $table->integer('condition');
            $table->string('location')->nullable();
            $table->string('class')->nullable();
            $table->string('imo_number')->nullable();
            $table->string('hp')->nullable();
            $table->string('measurement')->nullable();
            $table->string('lenght')->nullable();
            $table->string('breadth')->nullable();
            $table->string('depth')->nullable();
            $table->string('tonnage_net')->nullable();
            $table->string('tonnage_gross')->nullable();
            $table->string('light_weight')->nullable();
            $table->string('dead_weight')->nullable();
            $table->integer('price')->nullable();
            $table->integer('deleted')->nullable();
            $table->integer('delete_user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vessels');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCvExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cv_experiences', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('cv_id')->nullable();
            $table->string('company')->nullable();
            $table->string('position')->nullable();
            $table->string('responsibility')->nullable();
            $table->date('start_work')->nullable();
            $table->date('end_work')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cv_experiences');
    }
}

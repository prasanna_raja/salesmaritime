<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobCvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_cvs', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('job_category_id')->nullable();
            $table->integer('jobsub_category_id')->nullable();
            $table->string('title')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->integer('gender')->nullable();
            $table->date('date_add')->nullable();
            $table->date('date_expire')->nullable();
            $table->date('date_birth')->nullable();
            $table->integer('need_salary')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->integer('country')->nullable();
            $table->string('nationality')->nullable();
            $table->string('city')->nullable();
            $table->string('reloaction')->nullable();
            $table->string('lang_nat')->nullable();
            $table->string('cv_file')->nullable();
            $table->string('photo')->nullable();
            $table->date('deleted')->nullable();
            $table->date('date_del')->nullable();
            $table->integer('delete_user_id')->nullable();
            $table->integer('status')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_cvs');
    }
}

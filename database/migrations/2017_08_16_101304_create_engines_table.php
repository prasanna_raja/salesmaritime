<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEnginesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('engines', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->string('photo')->nullable();
            $table->string('protocol_file')->nullable();
            $table->integer('user_id');
            $table->text('description');
            $table->dateTime('date_add');
            $table->dateTime('date_del')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->integer('purchase')->nullable()->default(0);
            $table->integer('sold')->nullable()->default(0);
            $table->integer('year_of_build');
            $table->integer('country_build');
            $table->string('name');
            $table->string('model')->nullable();
            $table->string('make')->nullable();
            $table->integer('serial_number')->nullable();
            $table->string('part_number')->nullable();
            $table->string('rpm')->nullable();
            $table->string('kw')->nullable();
            $table->string('kva')->nullable();
            $table->string('hz')->nullable();
            $table->integer('condition');
            $table->string('location')->nullable();
            $table->integer('price')->nullable();
            $table->integer('deleted')->nullable();
            $table->integer('delete_user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('engines');
    }
}

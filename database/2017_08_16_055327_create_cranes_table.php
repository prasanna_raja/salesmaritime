<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCranesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cranes', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->string('photo')->nulllable();
            $table->string('protocol_file')->nulllable();
            $table->integer('user_id');
            $table->string('description');
            $table->dateTime('date_add');
            $table->dateTime('date_del')->nulllable();
            $table->integer('status')->default(1);
            $table->integer('purchase')->default(0);
            $table->integer('sold')->default(0);
            $table->integer('country_bulid');
            $table->string('year_of_build');
            $table->string('name');
            $table->string('make');
            $table->string('model');
            $table->string('tonn');
            $table->string('working_hours');
            $table->integer('condition');
            $table->string('location')->nulllable();
            $table->integer('price')->nulllable();
            $table->integer('deleted')->nulllable();
            $table->integer('delete_user_id')->nulllable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cranes');
    }
}

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Vessel {{ $vessel->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/vessels?type_id='.$type_id.'&cat_id='.$cat_id) }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/vessels/' . $vessel->id . '/edit?type_id='.$type_id.'&cat_id='.$cat_id) }}" title="Edit Vessel"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['vessels?type_id='.$type_id.'&cat_id='.$cat_id, $vessel->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Vessel',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $vessel->id }}</td>
                                    </tr>
                                    <tr><th> Category Id </th><td> {{ $vessel->category_id }} </td></tr><tr><th> Sub Category Id </th><td> {{ $vessel->sub_category_id }} </td></tr><tr><th> Photo </th><td> {{ $vessel->photo }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

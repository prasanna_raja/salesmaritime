<?php
    
    if(isset($charter->sub_category_id)){
       $charter_s_id =  $charter->sub_category_id;
    }else{
        $charter_s_id =  '';
    }

    if(isset($charter->photo)){
       $charter_photo =  $charter->photo;
    }else{
        $charter_photo =  '';
    }
    if(isset($charter->protocol_file)){
       $charter_protocol_file =  $charter->protocol_file;
    }else{
        $charter_protocol_file =  '';
    }
    if(isset($charter->country_build)){
       $charter_country_build =  $charter->country_build;
    }else{
        $charter_country_build =  '';
    }
  
  //echo '<pre>';print_r($categories) ;

  $charter_inq = 0;
  foreach ($categories as $category) {
     if($category->url == 'charter_inq'){
        
        $charter_inq = 1;

     }else{
       $charter_inq = 0;
     }
  }


    
?>


<input type="hidden" name="type_id" value="{{$type_id}}">

<div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
<!--     {!! Form::label('category_id', 'Category Id', ['class' => 'col-md-4 control-label']) !!}
 -->    <div class="col-md-6">
        <input type="hidden" name="category_id" value="{{$cat_id}}">

            @foreach ($categories as $category)
                @if($category->id == $cat_id)
                    {{$category->category_name}}
                @endif
            @endforeach 
        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('sub_category_id') ? 'has-error' : ''}}">
    {!! Form::label('sub_category_id', 'Sub Category Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="sub_category_id" class="form-control">
            <option>--select--</option>
        
            @foreach ($sub_categories as $sub_category)

                <option value="{{$sub_category->id}}" <?php if($sub_category->id == $charter_s_id ){ echo "selected";} ?> >{{$sub_category->category_name}}</option>

            @endforeach   

        </select>
        {!! $errors->first('sub_category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo', 'Photo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
            @if($charter_photo)
                <img class="img-responsive" style="height: 60px;" src="{{ url('/public/images/photo') }}/{{$charter->photo}}">
                {!! Form::file('photo', null, ['class' => 'form-control']) !!}
            @else
                {!! Form::file('photo', null, ['class' => 'form-control']) !!}
            @endif
        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('protocol_file') ? 'has-error' : ''}}">
    {!! Form::label('protocol_file', 'Protocol File', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
            @if($charter_protocol_file)
                <img class="img-responsive" style="height: 60px;" src="{{ url('/public/images/protocol_file') }}/{{$charter->protocol_file}}">
                {!! Form::file('protocol_file', null, ['class' => 'form-control']) !!}
            @else
                {!! Form::file('protocol_file', null, ['class' => 'form-control']) !!}
            @endif
        {!! $errors->first('protocol_file', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->
<input type="hidden" value="{{$user_id}}" name="user_id">

<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('date_add') ? 'has-error' : ''}}">
    {!! Form::label('date_add', 'Date Add', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_add', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_add', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('date_start') ? 'has-error' : ''}}">
    {!! Form::label('date_start', 'Date Start', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_start', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_start', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('date_end') ? 'has-error' : ''}}">
    {!! Form::label('date_end', 'Date End', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_end', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_end', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('status', null, ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('year_of_build') ? 'has-error' : ''}}">
    {!! Form::label('year_of_build', 'Year Of Build', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('year_of_build', null, ['class' => 'form-control']) !!}
        {!! $errors->first('year_of_build', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('country_build') ? 'has-error' : ''}}">
    {!! Form::label('country_build', 'Country Build', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="country_build" id="" class="form-control">
            <option>--select--</option>
            @foreach ( $countries as $country )
            <option value="{{$country->id}}" <?php if($country->id == $charter_country_build){ echo "selected"; } ?> >{{$country->country_name}}</option>
            @endforeach
        </select>
        {!! $errors->first('country_build', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->
<div class="form-group {{ $errors->has('condition') ? 'has-error' : ''}}">
    {!! Form::label('condition', 'Condition', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('condition', null, ['class' => 'form-control']) !!}
        {!! $errors->first('condition', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- <div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
    {!! Form::label('location', 'Location', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('location', null, ['class' => 'form-control']) !!}
        {!! $errors->first('location', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->
<div class="form-group {{ $errors->has('class') ? 'has-error' : ''}}">
    {!! Form::label('class', 'Class', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('class', null, ['class' => 'form-control']) !!}
        {!! $errors->first('class', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- <div class="form-group {{ $errors->has('imo_number') ? 'has-error' : ''}}">
    {!! Form::label('imo_number', 'Imo Number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('imo_number', null, ['class' => 'form-control']) !!}
        {!! $errors->first('imo_number', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->
<div class="form-group {{ $errors->has('crews') ? 'has-error' : ''}}">
    {!! Form::label('crews', 'Crews', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('crews', null, ['class' => 'form-control']) !!}
        {!! $errors->first('crews', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('propellers') ? 'has-error' : ''}}">
    {!! Form::label('propellers', 'Propellers', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('propellers', null, ['class' => 'form-control']) !!}
        {!! $errors->first('propellers', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('knote') ? 'has-error' : ''}}">
    {!! Form::label('knote', 'Knote', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('knote', null, ['class' => 'form-control']) !!}
        {!! $errors->first('knote', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('location_from') ? 'has-error' : ''}}">
    {!! Form::label('location_from', 'Location From', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('location_from', null, ['class' => 'form-control']) !!}
        {!! $errors->first('location_from', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('location_to') ? 'has-error' : ''}}">
    {!! Form::label('location_to', 'Location To', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('location_to', null, ['class' => 'form-control']) !!}
        {!! $errors->first('location_to', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('hp') ? 'has-error' : ''}}">
    {!! Form::label('hp', 'Hp', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('hp', null, ['class' => 'form-control']) !!}
        {!! $errors->first('hp', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('measurement') ? 'has-error' : ''}}">
    {!! Form::label('measurement', 'Measurement', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('measurement', null, ['class' => 'form-control']) !!}
        {!! $errors->first('measurement', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('length') ? 'has-error' : ''}}">
    {!! Form::label('length', 'Length', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('length', null, ['class' => 'form-control']) !!}
        {!! $errors->first('length', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('breadth') ? 'has-error' : ''}}">
    {!! Form::label('breadth', 'Breadth', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('breadth', null, ['class' => 'form-control']) !!}
        {!! $errors->first('breadth', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('depth') ? 'has-error' : ''}}">
    {!! Form::label('depth', 'Depth', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('depth', null, ['class' => 'form-control']) !!}
        {!! $errors->first('depth', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('tonnage_net') ? 'has-error' : ''}}">
    {!! Form::label('tonnage_net', 'Tonnage Net', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('tonnage_net', null, ['class' => 'form-control']) !!}
        {!! $errors->first('tonnage_net', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('tonnage_gross') ? 'has-error' : ''}}">
    {!! Form::label('tonnage_gross', 'Tonnage Gross', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('tonnage_gross', null, ['class' => 'form-control']) !!}
        {!! $errors->first('tonnage_gross', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('light_weight') ? 'has-error' : ''}}">
    {!! Form::label('light_weight', 'Light Weight', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('light_weight', null, ['class' => 'form-control']) !!}
        {!! $errors->first('light_weight', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- <div class="form-group {{ $errors->has('dead_weight') ? 'has-error' : ''}}">
    {!! Form::label('dead_weight', 'Dead Weight', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('dead_weight', null, ['class' => 'form-control']) !!}
        {!! $errors->first('dead_weight', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('price', null, ['class' => 'form-control']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

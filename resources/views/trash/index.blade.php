@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Trash : </div>
                    <div class="panel-body"> 

                        
                        <a href="trash?cat_id=1" class="btn btn-success btn-sm" title="Add New Yacht">
                            <i class="fa fa-plus" aria-hidden="true"></i> Vessels : {{$count_ves}}
                        </a>
                        <a href="trash?cat_id=2" class="btn btn-success btn-sm" title="Add New Yacht">
                            <i class="fa fa-plus" aria-hidden="true"></i> Yachts : {{$count_yac}}
                        </a>
                        <a href="trash?cat_id=3" class="btn btn-success btn-sm" title="Add New Yacht">
                            <i class="fa fa-plus" aria-hidden="true"></i> Marine Equipment : {{$count_mar}}
                        </a>
                        <a href="trash?cat_id=4" class="btn btn-success btn-sm" title="Add New Yacht">
                            <i class="fa fa-plus" aria-hidden="true"></i> Engines & Spare Parts : {{$count_eng}}
                        </a>
                        <a href="trash?cat_id=5" class="btn btn-success btn-sm" title="Add New Yacht">
                            <i class="fa fa-plus" aria-hidden="true"></i> Cranes : {{$count_cra}}
                        </a>
                        
                        {!! Form::open(['method' => 'GET', 'url' => '/vessels', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <input type="hidden" class="form-control curr_user_id" value="{{$user_id}}">
                            <input type="hidden" class="form-control cat_id" value="{{$cat_id}}">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Delete Date</th>
                                        <th>Photo</th>
                                        <th>Year</th>
                                        <th>Location</th>
                                        <th>Description</th>
                                        <th>price</th>
                                        <th>owner</th>
                                        <th>Deleted by</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($trashes as $item)
                                        <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->date_del }}</td>
                                        <td><img style="height: 50px;" class="img-responsive"  src="{{ url('/public/images/photo') }}/{{ $item->photo }}">
                                        </td>
                                        <td>{{ $item->year_of_build }}</td>
                                        <td>{{ $item->location }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->price }}</td>
                                        <td>{{ $item->user_id }}</td>
                                        <td>{{ $item->delete_user_id }}</td>
                                        <td>

                                            <button id="{{$item->id}}" class="btn btn-danger btn-xs restore_item"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Restore</button>
                                            
                                            <button id="{{$item->id}}" class="btn btn-danger btn-xs permanent_del"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Permanent Delete</button>
                                           
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $trashes->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('file') ? 'has-error' : ''}}">
    {!! Form::label('file', 'File', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('file', null, ['class' => 'form-control']) !!}
        {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('date_add') ? 'has-error' : ''}}">
    {!! Form::label('date_add', 'Date Add', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_add', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_add', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('date_del') ? 'has-error' : ''}}">
    {!! Form::label('date_del', 'Date Del', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_del', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_del', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('download') ? 'has-error' : ''}}">
    {!! Form::label('download', 'Download', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('download', null, ['class' => 'form-control']) !!}
        {!! $errors->first('download', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('delete_user_id') ? 'has-error' : ''}}">
    {!! Form::label('delete_user_id', 'Delete User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('delete_user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('delete_user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('deleted') ? 'has-error' : ''}}">
    {!! Form::label('deleted', 'Deleted', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('deleted', null, ['class' => 'form-control']) !!}
        {!! $errors->first('deleted', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('status', null, ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

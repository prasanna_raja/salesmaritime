@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">cranes</div>
                    <div class="panel-body">
                        <a href="{{ url('/cranes/create?type_id='. $type_id.'&cat_id='.$cat_id) }}" class="btn btn-success btn-sm" title="Add New Yacht">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/cranes', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <input type="hidden" class="form-control curr_user_id" value="{{$user_id}}">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Added</th>
                                        <th>Deleted</th>
                                        <th>Photo</th>
                                        <th>Year</th>
                                        <th>Location</th>
                                        <th>Description</th>
                                        <th>price</th>
                                        <th>owner</th>
                                        <th>status</th>
                                        <th>sold</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($cranes as $item)
                                    @if($item->deleted != 1)
                                        <tr>
                                    @else
                                        <tr style="background-color: #778899;">
                                    @endif
                                       <td>{{ $item->id }}</td>
                                        <td>{{ $item->date_add }}</td>
                                        <td>{{ $item->date_del }}</td>
                                        <td><img style="height: 50px;" class="img-responsive"  src="{{ url('/public/images/photo') }}/{{ $item->photo }}">
                                        </td>
                                        <td>{{ $item->year_of_build }}</td>
                                        <td>{{ $item->location }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->price }}</td>
                                        <td><a href="{{ url('/users/'.$item->user_id.'') }}">{{ $item->first_name }}-{{ $item->last_name }}</a></td>
                                        <td>
                                        @if($item->status == 1)
                                            Active
                                        @else
                                            Inactive
                                        @endif
                                        </td>
                                        <td>{{ $item->sold }}</td>
                                        <td>
                                            <!-- <a href="{{ url('/cranes/' . $item->id) }}" title="View Yacht"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a> -->
                                            <a href="{{ url('/cranes/' . $item->id . '/edit'.'?type_id='.$type_id.'&cat_id='.$cat_id) }}" title="Edit Yacht"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <button id="{{ $item->id }}" class="btn btn-danger btn-xs del_item_cranes"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Delete</button>

                                            <!-- {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/cranes?type_id='.$type_id.'&cat_id='.$cat_id, $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Yacht',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!} -->
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $cranes->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="col-md-3">
    <div class="panel panel-default panel-flush">
        <div class="panel-heading">
            Sidebar
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/home') }}">
                        Dashboard
                    </a>
                </li>
            </ul>
        </div>

        <div class="panel-heading">
            Categories
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/types') }}">
                        Type
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/categories') }}">
                        categories
                    </a>
                </li>
                <!-- <li role="presentation">
                    <a href="{{ url('/sub-categories') }}">
                        Sub categories
                    </a>
                </li> -->
            </ul>
        </div>


        @foreach ($get_cat as $category)
            <div class="panel-heading">
                {{$category->category_name}}
            </div>
            <?php
                $id = $category->id;
                $s_cats = DB::table('categories')->where('parent_id', '=',$id)->get();
            ?>
            
                <div class="panel-body">
                    <ul class="nav" role="tablist">
                        @foreach ($s_cats as $s_cat)     
                        <?php $cat_id = $s_cat->id; ?>               
                        <li role="presentation">
                            <a href="{{ url('/'.$s_cat->url.'?type_id='.$id.'&cat_id='.$cat_id) }}">
                                {{$s_cat->category_name}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>   
        @endforeach

        <div class="panel-heading">
            Trash
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/trash?cat_id=1') }}">
                        Items Trash
                    </a>
                </li>
            </ul>
        </div>

        <div class="panel-heading">
            Directory
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/dir-category') }}">
                        Directory category
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/dir_items') }}">
                        Directory Items
                    </a>
                </li>
                
                
            </ul>
        </div>

        <div class="panel-heading">
            Jobs and CV
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/job-category') }}">
                        Job category
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/jobs') }}">
                        Add/Post Jobs
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/job-cv') }}">
                        Add/List CV 
                    </a>
                </li>
                
            </ul>
        </div>

        <div class="panel-heading">
            Tenders
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/tenders') }}">
                       Tenders
                    </a>
                </li>
                
            </ul>
        </div>

        <div class="panel-heading">
            Sponsors
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/sponsors-logo') }}">
                       Sponsors
                    </a>
                </li>
                
            </ul>
        </div>


    </div>
</div>

<div class="form-group {{ $errors->has('cv_id') ? 'has-error' : ''}}">
    {!! Form::label('cv_id', 'Cv Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('cv_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('cv_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('education_id') ? 'has-error' : ''}}">
    {!! Form::label('education_id', 'Education Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('education_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('education_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('department') ? 'has-error' : ''}}">
    {!! Form::label('department', 'Department', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('department', null, ['class' => 'form-control']) !!}
        {!! $errors->first('department', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('dip_deg') ? 'has-error' : ''}}">
    {!! Form::label('dip_deg', 'Dip Deg', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('dip_deg', null, ['class' => 'form-control']) !!}
        {!! $errors->first('dip_deg', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('year_end') ? 'has-error' : ''}}">
    {!! Form::label('year_end', 'Year End', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('year_end', null, ['class' => 'form-control']) !!}
        {!! $errors->first('year_end', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

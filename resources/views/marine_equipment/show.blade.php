@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Marine_equipment {{ $marine_equipment->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/marine_equipment?type_id='.$type_id.'&cat_id='.$cat_id) }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/marine_equipment/' . $marine_equipment->id . '/edit?type_id='.$type_id.'&cat_id='.$cat_id)  }}" title="Edit Marine_equipment"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['marine_equipment?type_id='.$type_id.'&cat_id='.$cat_id, $marine_equipment->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Marine_equipment',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $marine_equipment->id }}</td>
                                    </tr>
                                    <tr><th> Category Id </th><td> {{ $marine_equipment->category_id }} </td></tr><tr><th> Sub Category Id </th><td> {{ $marine_equipment->sub_category_id }} </td></tr><tr><th> Photo </th><td> {{ $marine_equipment->photo }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

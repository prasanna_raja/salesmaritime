<!-- <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->
<?php
    foreach ($users as $user) {
        $first_name = $user->first_name;
        $last_name = $user->last_name;
        $email = $user->email;
    }
    //echo '<pre>';print_r($jobcv);
?>
<input type="hidden"  value="{{json_encode($educations)}}" id="edu" >
<input type="hidden" name="user_id" value="{{$user_id}}">
<div class="form-group {{ $errors->has('job_category_id') ? 'has-error' : ''}}">
    {!! Form::label('job_category_id', 'Job Category Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="job_category_id" class="form-control">
            <option value="0">--SELECT--</option>
            @foreach ($job_categories as $job_category)
                <option value="{{$job_category->id}}" <?php if(isset($jobcv)){if($jobcv->job_category_id == $job_category->id){ echo "Selected";}} ?> >{{$job_category->jobcategory_name}}</option>
            @endforeach
        </select>
        {!! $errors->first('job_category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('jobsub_category_id') ? 'has-error' : ''}}">
    {!! Form::label('jobsub_category_id', 'Jobsub Category Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="jobsub_category_id" class="form-control">
            <option value="0">--SELECT--</option>
            @foreach ($jobsub_categories as $jobsub_category)
                <option value="{{$jobsub_category->id}}" <?php if(isset($jobcv)){if($jobcv->jobsub_category_id == $jobsub_category->id){ echo "Selected";}} ?> >{{$jobsub_category->jobcategory_name}}</option>
            @endforeach
        </select>
        {!! $errors->first('jobsub_category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    {!! Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="text" name="first_name" value="{{$first_name}}" class="form-control">
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    {!! Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="text" name="last_name" value="{{$last_name}}" class="form-control">
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
    {!! Form::label('gender', 'Gender', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="radio" name="gender" value="0" checked>Male
        <input type="radio" name="gender" value="1">Female

        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('date_add') ? 'has-error' : ''}}">
    {!! Form::label('date_add', 'Date Add', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_add', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_add', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('date_expire') ? 'has-error' : ''}}">
    {!! Form::label('date_expire', 'Date Expire', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_expire', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_expire', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('date_birth') ? 'has-error' : ''}}">
    {!! Form::label('date_birth', 'Date Birth', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_birth', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_birth', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('need_salary') ? 'has-error' : ''}}">
    {!! Form::label('need_salary', 'Need Salary', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('need_salary', null, ['class' => 'form-control']) !!}
        {!! $errors->first('need_salary', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="text" name="email" value="{{$email}}" class="form-control">
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', 'Mobile', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', 'Country', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="country" class="form-control">
            <option value="0">--SELECT--</option>
            @foreach ($countries as $country)
                <option value="{{$country->id}}" <?php if(isset($jobcv)){if($jobcv->country == $country->id){ echo "Selected";}} ?>>{{$country->country_name}}</option>
            @endforeach
        </select>
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('nationality') ? 'has-error' : ''}}">
    {!! Form::label('nationality', 'Nationality', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nationality', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nationality', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', 'City', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('city', null, ['class' => 'form-control']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('reloaction') ? 'has-error' : ''}}">
    {!! Form::label('reloaction', 'Reloaction', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="radio" name="reloaction" checked="" value="1">Yes
        <input type="radio" name="reloaction" value="2">No
        <input type="radio" name="reloaction" value="3">It's Desirable
        {!! $errors->first('reloaction', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('lang_nat') ? 'has-error' : ''}}">
    {!! Form::label('lang_nat', 'Lang Nat', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('lang_nat', null, ['class' => 'form-control']) !!}
        {!! $errors->first('lang_nat', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('cv_file') ? 'has-error' : ''}}">
    {!! Form::label('cv_file', 'Cv File', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('cv_file', null, ['class' => 'form-control']) !!}
        {!! $errors->first('cv_file', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo', 'Photo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('photo', null, ['class' => 'form-control']) !!}
        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('Education', 'education', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="input_fields_wrap">
            <div class="edu_fields">

            @if(isset($cv_educations))

                <button class="add_field_button">Add More Education</button>
                <?php $edu = 0; ?>
                @foreach ($cv_educations as $cv_education)
                    <select class="eduSelect0 form-control" name="education_id[]">
                        <option>Select Level Of Education</option>
                        @foreach ($educations as $education)
                            <option value="{{$education->id}}" <?php if($cv_education->education_id == $education->id){ echo "Selected"; } ?> >{{$education->name}}</option>
                        @endforeach
                    </select>
                    <input type="text" class="form-control" value="{{$cv_education->name}}" name="name[]" placeholder="Name of school">
                    <input type="text" class="form-control" value="{{$cv_education->department}}" name="department[]" placeholder="Faculty / Department">
                    <input type="text" class="form-control" value="{{$cv_education->dip_deg}}" name="dip_deg[]" placeholder="Diploma / Degree">
                    <input type="text" class="form-control" value="{{$cv_education->year_end}}" name="year_end[]" placeholder="Year of end">
                    <a href="#" class="remove_field">Remove Education</a>
                    <?php $edu++; ?>
                @endforeach


            @else

                <select class="eduSelect0 form-control" name="education_id[]">
                    <option>Select Level Of Education</option>
                        @foreach ($educations as $education)
                            <option value="{{$education->id}}" >{{$education->name}}</option>
                        @endforeach
                </select>
                <input type="text" class="form-control" name="name[]" placeholder="Name of school">
                <input type="text" class="form-control" name="department[]" placeholder="Faculty / Department">
                <input type="text" class="form-control" name="dip_deg[]" placeholder="Diploma / Degree">
                <input type="text" class="form-control" name="year_end[]" placeholder="Year of end">
                <button class="add_field_button">Add More Education</button>

            @endif
                
                

            </div>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('Professional Experience', 'experience', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="exp_wrap">
            <div class="edu_fields">

            @if(isset($cv_experiences))
                <button class="add_exp_button">Add More Experience</button>
                <?php $exp = 0; ?>
                @foreach ($cv_experiences as $cv_experience)
                    <input type="text" class="form-control" value="{{$cv_experience->company}}" name="company[]" placeholder="Company Name">
                    <input type="text" class="form-control" value="{{$cv_experience->position}}" name="position[]" placeholder="Tile/Position">
                    <input type="text" class="form-control" value="{{$cv_experience->responsibility}}" name="responsibility[]" placeholder="Responsibilities, functions, achievements">
                    Start Work<input type="Date" class="form-control" value="{{$cv_experience->start_work}}" name="start_work[]" >
                    End Work<input type="Date" class="form-control" value="{{$cv_experience->end_work}}" name="end_work[]" >
                    <a href="#" class="exp_remove_field">Remove experience</a>
                    <?php $exp++; ?>
                @endforeach


            @else
                <input type="text" class="form-control" name="company[]" placeholder="Company Name">
                <input type="text" class="form-control" name="position[]" placeholder="Tile/Position">
                <input type="text" class="form-control" name="responsibility[]" placeholder="Responsibilities, functions, achievements">
                Start Work<input type="Date" class="form-control" name="start_work[]" >
                End Work<input type="Date" class="form-control" name="end_work[]" >
                <button class="add_exp_button">Add More Experience</button>
            @endif
                
                

            </div>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('Languages', 'launguage', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="lang_wrap">
            <div class="edu_fields">
                @if(isset($cv_launguages))

                    <button class="add_lang_button">Add More Launguages</button>

                    <?php $l = 0; ?>
                    @foreach($cv_launguages as $cv_launguage)
                        <input type="text" value="{{$cv_launguage->lang}}" class="form-control" name="lang[]" placeholder="Launguage Name">
                        <select name="level[]" class="form-control">
                            <option value="0" <?php if($cv_launguage->level == 0){ echo "Selected"; } ?> >Don't known</option>
                            <option value="1" <?php if($cv_launguage->level == 1){ echo "Selected"; } ?> >Basic</option>
                            <option value="2" <?php if($cv_launguage->level == 2){ echo "Selected"; } ?> >Read professional documentation</option>
                            <option value="3" <?php if($cv_launguage->level == 3){ echo "Selected"; } ?> >Can be interviewed</option>
                            <option value="4" <?php if($cv_launguage->level == 4){ echo "Selected"; } ?> >Free</option>
                            <option value="5" <?php if($cv_launguage->level == 5){ echo "Selected"; } ?> >Native</option>
                            
                        </select>
                        <a href="#" class="lang_remove_field">Remove Launguages</a>
                        <?php $l++; ?>
                    @endforeach
                @else
                    <input type="text" class="form-control" name="lang[]" placeholder="Launguage Name">
                    <select name="level[]" class="form-control">
                        <option value="0">Don't known</option>
                        <option value="1">Basic</option>
                        <option value="2">Read professional documentation</option>
                        <option value="3">Can be interviewed</option>
                        <option value="4">Free</option>
                        <option value="5">Native</option>
                    </select>
                    <button class="add_lang_button">Add More Launguages</button>

                @endif
                


            </div>
        </div>
    </div>
</div>


<!-- <div class="form-group {{ $errors->has('deleted') ? 'has-error' : ''}}">
    {!! Form::label('deleted', 'Deleted', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('deleted', null, ['class' => 'form-control']) !!}
        {!! $errors->first('deleted', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('date_del') ? 'has-error' : ''}}">
    {!! Form::label('date_del', 'Date Del', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_del', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_del', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('delete_user_id') ? 'has-error' : ''}}">
    {!! Form::label('delete_user_id', 'Delete User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('delete_user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('delete_user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('status', null, ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<div class="form-group {{ $errors->has('job_type_name') ? 'has-error' : ''}}">
    {!! Form::label('job_type_name', 'Job Type Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('job_type_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('job_type_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

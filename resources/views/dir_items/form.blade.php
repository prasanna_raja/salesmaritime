<div class="form-group {{ $errors->has('date_add') ? 'has-error' : ''}}">
    {!! Form::label('date_add', 'Date Add', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_add', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_add', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('keywords') ? 'has-error' : ''}}">
    {!! Form::label('keywords', 'Keywords', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('keywords', null, ['class' => 'form-control']) !!}
        {!! $errors->first('keywords', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('dir_category_id') ? 'has-error' : ''}}">
    {!! Form::label('dir_category_id', 'Category', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="dir_category_id" class="form-control">
            <option></option>
            @foreach ($dir_categories as $dir_category)
                <option value="{{$dir_category->id}}">{{$dir_category->dir_category_name}}</option>
            @endforeach
        </select>
        
<!--         {!! Form::number('dir_category_id', null, ['class' => 'form-control']) !!}
 -->        {!! $errors->first('dir_category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('dir_subcategory_id') ? 'has-error' : ''}}">
    {!! Form::label('dir_subcategory_id', 'Subcategory', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="dir_subcategory_id" class="form-control">
            <option></option>
            @foreach ($dir_subcategories as $dir_subcategory)
                <option value="{{$dir_subcategory->id}}">{{$dir_subcategory->dir_category_name}}</option>
            @endforeach
        </select>        
        {!! $errors->first('dir_subcategory_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @foreach($users as $user)
            <?php 
                
                $user_id = $user->id;
                $first_name = $user->first_name;
                $last_name = $user->last_name;

            ?>
        @endforeach
        {{$first_name}} {{$last_name}}<input type="hidden" value="{{$user_id}}" name="user_id">
<!--         {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
 -->        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('status', null, ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

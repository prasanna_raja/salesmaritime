@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Dir_items</div>
                    <div class="panel-body">
                        @if($already_dir == 0)
                        <a href="{{ url('/dir_items/create') }}" class="btn btn-success btn-sm" title="Add New dir_item">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        @else
                        <a href="{{ url('/dir_items/' . $item_id . '/edit') }}" class="btn btn-success btn-sm" >
                            <i class="fa fa-plus" aria-hidden="true"></i> Edit Directory
                        </a>
                        @endif
                        

                        {!! Form::open(['method' => 'GET', 'url' => '/dir_items', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Keywords</th>
                                        <th>Description</th>
                                        <th>Categories</th>
                                        <th>Sub Categories</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($dir_items as $item)
                                    <tr>
                                        <td>
                                        @foreach ($users as $user)
                                            @if($item->user_id == $user->id)
                                                {{$user->first_name}} {{$user->last_name}}
                                            @endif
                                        @endforeach
                                        </td>
                                        <td>{{ $item->keywords }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>
                                        @foreach ($dir_categories as $dir_category)
                                            @if($item->dir_category_id == $dir_category->id)
                                                {{$dir_category->dir_category_name}}
                                            @endif
                                        @endforeach
                                        </td>
                                        <td>
                                        @foreach ($dir_subcategories as $dir_subcategory)
                                            @if($item->dir_subcategory_id == $dir_subcategory->id)
                                                {{$dir_subcategory->dir_category_name}}
                                            @endif
                                        @endforeach
                                        </td>
                                        <td>
                                            <a href="{{ url('/dir_items/' . $item->id) }}" title="View dir_item"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/dir_items/' . $item->id . '/edit') }}" title="Edit dir_item"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/dir_items', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete dir_item',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $dir_items->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

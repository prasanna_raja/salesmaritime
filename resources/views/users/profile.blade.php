@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <?php         


           // print_r($profile);

            foreach ($profile as $user) {

                $user_id = $user->id;
                $first_name = $user->first_name;
                $last_name = $user->last_name;
                $company_name = $user->company_name;
                $user_name = $user->user_name;
                $title_position = $user->title_position;
                $country_id = $user->country_id;
                $address = $user->address;
                $address2 = $user->address2;
                $city = $user->city;
                $tel = $user->tel;
                $fax = $user->fax;
                $mobile_number = $user->mobile_number;
                $website = $user->website;
                $email = $user->email;
                $password = $user->password;
                $po_box = $user->po_box;
                $newsletter = $user->newsletter;
            }

            ?>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">Profile Update</div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="POST" action="updateProfile">
                                     {{ csrf_field() }}
                                    <input id="user_id" type="hidden" class="form-control" name="user_id" value="{{$user_id}}"  autofocus>
                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label for="first_name" class="col-md-4 control-label">First Name</label>

                                        <div class="col-md-6">
                                            <input id="first_name" type="text" class="form-control" name="first_name" value="{{$first_name}}" required autofocus>

                                            @if ($errors->has('first_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label for="last_name" class="col-md-4 control-label">Last Name</label>

                                        <div class="col-md-6">
                                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{$last_name}}" required autofocus>

                                            @if ($errors->has('last_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                                        <label for="user_name" class="col-md-4 control-label">User Name</label>

                                        <div class="col-md-6">
                                            <input id="user_name" type="text" class="form-control" name="user_name" value="{{$user_name}}" required autofocus>

                                            @if ($errors->has('user_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('user_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email" value="{{$email}}" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Password</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password" >

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    

                                    <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                                        <label for="company_name" class="col-md-4 control-label">Company Name</label>

                                        <div class="col-md-6">
                                            <input id="company_name" type="text" class="form-control" name="company_name" value="{{$company_name}}" required autofocus>

                                            @if ($errors->has('company_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('company_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('title_position') ? ' has-error' : '' }}">
                                        <label for="title_position" class="col-md-4 control-label">Title Position</label>

                                        <div class="col-md-6">
                                            <input id="title_position" type="text" class="form-control" name="title_position" value="{{$title_position}}" required autofocus>

                                            @if ($errors->has('title_position'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('title_position') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                                        <label for="country_id" class="col-md-4 control-label">Country</label>

                                        <div class="col-md-6">

                                            <select id="country_id" class="form-control" name="country_id" required>
                                                <option value="0">--select--</option>

                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}" <?php if($country_id == $country->id){ echo "selected"; } ?> >{{$country->country_name}}</option>
                                                @endforeach
                                               
                                                
                                            </select>

                                            @if ($errors->has('country_id'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('country_id') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label for="city" class="col-md-4 control-label">City</label>

                                        <div class="col-md-6">
                                            <input id="city" type="text" class="form-control" name="city" value="{{$city}}" required autofocus>

                                            @if ($errors->has('city'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label for="address" class="col-md-4 control-label">Address</label>

                                        <div class="col-md-6">
                                            <input id="address" type="text" class="form-control" name="address" value="{{$address}}" required autofocus>

                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}">
                                        <label for="address2" class="col-md-4 control-label">Address2</label>

                                        <div class="col-md-6">
                                            <input id="address2" type="text" class="form-control" name="address2" value="{{$address2}}" required autofocus>

                                            @if ($errors->has('address2'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address2') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('po_box') ? ' has-error' : '' }}">
                                        <label for="po_box" class="col-md-4 control-label">P.O Box</label>

                                        <div class="col-md-6">
                                            <input id="po_box" type="number" class="form-control" name="po_box" value="{{$po_box}}" required autofocus>

                                            @if ($errors->has('po_box'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('po_box') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
                                        <label for="tel" class="col-md-4 control-label">Telephone</label>

                                        <div class="col-md-6">
                                            <input id="tel" type="number" class="form-control" name="tel" value="{{$tel}}" required autofocus>

                                            @if ($errors->has('tel'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('tel') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group{{ $errors->has('fax') ? ' has-error' : '' }}">
                                        <label for="fax" class="col-md-4 control-label">Fax</label>

                                        <div class="col-md-6">
                                            <input id="fax" type="number" class="form-control" name="fax" value="{{$fax}}" required autofocus>

                                            @if ($errors->has('fax'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('fax') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                        <label for="mobile_number" class="col-md-4 control-label">Mobile Number</label>

                                        <div class="col-md-6">
                                            <input id="mobile_number" type="number" class="form-control" name="mobile_number" value="{{$mobile_number}}" required autofocus>

                                            @if ($errors->has('mobile_number'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('mobile_number') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                                        <label for="website" class="col-md-4 control-label">Website</label>

                                        <div class="col-md-6">
                                            <input id="website" type="text" class="form-control" name="website" value="{{$website}}" autofocus>

                                            @if ($errors->has('website'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('website') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group{{ $errors->has('newsletter') ? ' has-error' : '' }}">
                                        <label for="newsletter" class="col-md-4 control-label">Newsletter</label>

                                        <div class="col-md-6">
                                            <input id="newsletter" type="radio" class="" name="newsletter" value="1" <?php if($newsletter == 1){ echo "checked"; } ?> >Yes
                                            <input id="newsletter" type="radio" class="" name="newsletter" value="0" <?php if($newsletter == 0){ echo "checked"; } ?> >No

                                            @if ($errors->has('newsletter'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('newsletter') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

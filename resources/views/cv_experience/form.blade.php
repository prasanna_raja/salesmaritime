<div class="form-group {{ $errors->has('cv_id') ? 'has-error' : ''}}">
    {!! Form::label('cv_id', 'Cv Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('cv_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('cv_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
    {!! Form::label('company', 'Company', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('company', null, ['class' => 'form-control']) !!}
        {!! $errors->first('company', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('position') ? 'has-error' : ''}}">
    {!! Form::label('position', 'Position', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('position', null, ['class' => 'form-control']) !!}
        {!! $errors->first('position', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('responsibility') ? 'has-error' : ''}}">
    {!! Form::label('responsibility', 'Responsibility', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('responsibility', null, ['class' => 'form-control']) !!}
        {!! $errors->first('responsibility', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('start_work') ? 'has-error' : ''}}">
    {!! Form::label('start_work', 'Start Work', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('start_work', null, ['class' => 'form-control']) !!}
        {!! $errors->first('start_work', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('end_work') ? 'has-error' : ''}}">
    {!! Form::label('end_work', 'End Work', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('end_work', null, ['class' => 'form-control']) !!}
        {!! $errors->first('end_work', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

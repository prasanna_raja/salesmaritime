<div class="form-group {{ $errors->has('date_add') ? 'has-error' : ''}}">
    {!! Form::label('date_add', 'Date Add', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('date_add', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_add', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->
<input type="hidden" name="user_id" value="{{$user_id}}">
<div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
    {!! Form::label('company', 'Company', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('company', null, ['class' => 'form-control']) !!}
        {!! $errors->first('company', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('job_category_id') ? 'has-error' : ''}}">
    {!! Form::label('job_category_id', 'Job Category Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="job_category_id" class="form-control">
            <option value="0">--select--</option>
            @foreach ($job_categories as $job_category)
                    <option value="{{$job_category->id}}" <?php if($job->job_category_id == $job_category->id){ echo "selected";} ?> >{{$job_category->jobcategory_name}}</option>
            @endforeach
            
        </select>
        {!! $errors->first('job_category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('jobsub_category_id') ? 'has-error' : ''}}">
    {!! Form::label('jobsub_category_id', 'Jobsub Category Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
    <select name="jobsub_category_id" class="form-control">
            <option value="0">--select--</option>
            @foreach ($jobsub_categories as $jobsub_category)
                <option value="{{$jobsub_category->id}}" <?php if($job->jobsub_category_id == $jobsub_category->id){ echo "selected";} ?> >{{$jobsub_category->jobcategory_name}}</option>
            @endforeach
            
        </select>
        {!! $errors->first('jobsub_category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('job_type') ? 'has-error' : ''}}">
    {!! Form::label('job_type', 'Job Type', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="job_type" class="form-control">
            <option value="0">--select--</option>
            @foreach ($job_types as $job_type)
                <option value="{{$job_type->id}}" <?php if($job->job_type == $job_type->id){ echo "selected";} ?>>{{$job_type->job_type_name}}</option>
            @endforeach
            
        </select>
        {!! $errors->first('job_type', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('experience') ? 'has-error' : ''}}">
    {!! Form::label('experience', 'Experience', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="experience" class="form-control">
            <option value="0">--select--</option>
            @foreach ($job_experiences as $job_experience)
                <option value="{{$job_experience->id}}" <?php if($job->experience == $job_experience->id){ echo "selected";} ?>>{{$job_experience->job_exp_name}}</option>
            @endforeach
            
        </select>
        {!! $errors->first('experience', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('req_skill') ? 'has-error' : ''}}">
    {!! Form::label('req_skill', 'Req Skill', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('req_skill', null, ['class' => 'form-control']) !!}
        {!! $errors->first('req_skill', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', 'City', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('city', null, ['class' => 'form-control']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', 'Country', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="country" class="form-control">
            <option value="0">--select--</option>
            @foreach ($countries as $country)
                <option value="{{$country->id}}" <?php if($job->country == $country->id){ echo "selected";} ?> >{{$country->country_name}}</option>
            @endforeach
            
        </select>
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('rem_currency') ? 'has-error' : ''}}">
    {!! Form::label('rem_currency', 'Rem Currency', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="rem_currency" class="form-control">
            <option value="0">--select--</option>
            @foreach ($countries as $country)
                <option value="{{$country->id}}" <?php if($job->rem_currency == $country->id){ echo "selected";} ?>>{{$country->currency}}</option>
            @endforeach
            
        </select>
        {!! $errors->first('rem_currency', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('rem_min') ? 'has-error' : ''}}">
    {!! Form::label('rem_min', 'Rem Min', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('rem_min', null, ['class' => 'form-control']) !!}
        {!! $errors->first('rem_min', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('rem_max') ? 'has-error' : ''}}">
    {!! Form::label('rem_max', 'Rem Max', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('rem_max', null, ['class' => 'form-control']) !!}
        {!! $errors->first('rem_max', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('close_date') ? 'has-error' : ''}}">
    {!! Form::label('close_date', 'Close Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('close_date', null, ['class' => 'form-control']) !!}
        {!! $errors->first('close_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

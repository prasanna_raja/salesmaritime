<?php

    if(isset($engine->sub_category_id)){
       $engine_s_id =  $engine->sub_category_id;
    }else{
        $engine_s_id =  '';
    }

    if(isset($engine->photo)){
       $engine_photo =  $engine->photo;
    }else{
        $engine_photo =  '';
    }
    if(isset($engine->protocol_file)){
       $engine_protocol_file =  $engine->protocol_file;
    }else{
        $engine_protocol_file =  '';
    }
    if(isset($engine->country_build)){
       $engine_country_build =  $engine->country_build;
    }else{
        $engine_country_build =  '';
    }

?>
<input type="hidden" name="type_id" value="{{$type_id}}">

<div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
<!--     {!! Form::label('category_id', 'Category Id', ['class' => 'col-md-4 control-label']) !!}
 -->    <div class="col-md-6">
    <input type="hidden" name="category_id" value="{{$cat_id}}">

    @foreach ($categories as $category)
        @if($category->id == $cat_id)
            {{$category->category_name}}
        @endif
    @endforeach 
<!--         {!! Form::number('category_id', null, ['class' => 'form-control']) !!}
 -->        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('sub_category_id') ? 'has-error' : ''}}">
    {!! Form::label('sub_category_id', 'Sub Category Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="sub_category_id" class="form-control">
            <option>--select--</option>
        
            @foreach ($sub_categories as $sub_category)

                <option value="{{$sub_category->id}}" <?php if($sub_category->id == $engine_s_id ){ echo "selected";} ?> >{{$sub_category->category_name}}</option>

            @endforeach   

        </select>        
        {!! $errors->first('sub_category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if ($type_id == 1)
<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo', 'Photo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if($engine_photo)
            <img class="img-responsive" style="height: 60px;" src="{{ url('/public/images/photo') }}/{{$engine->photo}}">
            {!! Form::file('photo', null, ['class' => 'form-control']) !!}
        @else
            {!! Form::file('photo', null, ['class' => 'form-control']) !!}
        @endif        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('protocol_file') ? 'has-error' : ''}}">
    {!! Form::label('protocol_file', 'Protocol File', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if($engine_protocol_file)
            <img class="img-responsive" style="height: 60px;" src="{{ url('/public/images/protocol_file') }}/{{$engine->protocol_file}}">
            {!! Form::file('protocol_file', null, ['class' => 'form-control']) !!}
        @else
            {!! Form::file('protocol_file', null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('protocol_file', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@else
@endif
<!-- <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->
<input type="hidden" value="{{$user_id}}" name="user_id">

<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('date_add') ? 'has-error' : ''}}">
    {!! Form::label('date_add', 'Date Add', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('datetime-local', 'date_add', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_add', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="radio" value="1" checked name="status">Active
        <input type="radio" value="0" name="status">Inactive
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('year_of_build') ? 'has-error' : ''}}">
    {!! Form::label('year_of_build', 'Year Of Build', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('year_of_build', null, ['class' => 'form-control']) !!}
        {!! $errors->first('year_of_build', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('country_build') ? 'has-error' : ''}}">
    {!! Form::label('country_build', 'Country Build', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="country_build" id="" class="form-control">
            <option>--select--</option>
            @foreach ( $countries as $country )
            <option value="{{$country->id}}" <?php if($country->id == $engine_country_build){ echo "selected"; } ?> >{{$country->country_name}}</option>
            @endforeach
        </select>
        {!! $errors->first('country_build', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('model') ? 'has-error' : ''}}">
    {!! Form::label('model', 'Model', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('model', null, ['class' => 'form-control']) !!}
        {!! $errors->first('model', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('make') ? 'has-error' : ''}}">
    {!! Form::label('make', 'Make', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('make', null, ['class' => 'form-control']) !!}
        {!! $errors->first('make', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('serial_number') ? 'has-error' : ''}}">
    {!! Form::label('serial_number', 'Serial Number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('serial_number', null, ['class' => 'form-control']) !!}
        {!! $errors->first('serial_number', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('part_number') ? 'has-error' : ''}}">
    {!! Form::label('part_number', 'Part Number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('part_number', null, ['class' => 'form-control']) !!}
        {!! $errors->first('part_number', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('rpm') ? 'has-error' : ''}}">
    {!! Form::label('rpm', 'Rpm', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('rpm', null, ['class' => 'form-control']) !!}
        {!! $errors->first('rpm', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('kw') ? 'has-error' : ''}}">
    {!! Form::label('kw', 'Kw', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('kw', null, ['class' => 'form-control']) !!}
        {!! $errors->first('kw', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('kva') ? 'has-error' : ''}}">
    {!! Form::label('kva', 'Kva', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('kva', null, ['class' => 'form-control']) !!}
        {!! $errors->first('kva', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('hz') ? 'has-error' : ''}}">
    {!! Form::label('hz', 'Hz', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('hz', null, ['class' => 'form-control']) !!}
        {!! $errors->first('hz', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('condition') ? 'has-error' : ''}}">
    {!! Form::label('condition', 'Condition', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('condition', null, ['class' => 'form-control']) !!}
        {!! $errors->first('condition', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
    {!! Form::label('location', 'Location', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('location', null, ['class' => 'form-control']) !!}
        {!! $errors->first('location', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('price', null, ['class' => 'form-control']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

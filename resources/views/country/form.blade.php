<div class="form-group {{ $errors->has('country_name') ? 'has-error' : ''}}">
    {!! Form::label('country_name', 'Country Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('country_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('country_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('country_code') ? 'has-error' : ''}}">
    {!! Form::label('country_code', 'Country Code', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('country_code', null, ['class' => 'form-control']) !!}
        {!! $errors->first('country_code', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('currency') ? 'has-error' : ''}}">
    {!! Form::label('currency', 'Currency', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('currency', null, ['class' => 'form-control']) !!}
        {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<div class="form-group {{ $errors->has('cv_id') ? 'has-error' : ''}}">
    {!! Form::label('cv_id', 'Cv Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('cv_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('cv_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('lang') ? 'has-error' : ''}}">
    {!! Form::label('lang', 'Lang', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('lang', null, ['class' => 'form-control']) !!}
        {!! $errors->first('lang', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('level') ? 'has-error' : ''}}">
    {!! Form::label('level', 'Level', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('level', null, ['class' => 'form-control']) !!}
        {!! $errors->first('level', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<?php
    
    if(isset($yacht->sub_category_id)){
       $yacht_s_id =  $yacht->sub_category_id;
    }else{
        $yacht_s_id =  '';
    }

    if(isset($yacht->photo)){
       $yacht_photo =  $yacht->photo;
    }else{
        $yacht_photo =  '';
    }
    if(isset($yacht->protocol_file)){
       $yacht_protocol_file =  $yacht->protocol_file;
    }else{
        $yacht_protocol_file =  '';
    }
    if(isset($yacht->country_build)){
       $yacht_country_build =  $yacht->country_build;
    }else{
        $yacht_country_build =  '';
    }
    
    
?>
<input type="hidden" name="type_id" value="{{$type_id}}">

<div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
<!--     {!! Form::label('category_id', 'Category Id', ['class' => 'col-md-4 control-label']) !!}
 -->    <div class="col-md-6">
             <input type="hidden" name="category_id" value="{{$cat_id}}">

                @foreach ($categories as $category)
                    @if($category->id == $cat_id)
                        {{$category->category_name}}
                    @endif
                @endforeach 
        <!-- {!! Form::number('category_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!} -->
    </div>
</div><div class="form-group {{ $errors->has('sub_category_id') ? 'has-error' : ''}}">
    {!! Form::label('sub_category_id', 'Sub Category Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="sub_category_id" class="form-control">
            <option>--select--</option>
        
            @foreach ($sub_categories as $sub_category)

                <option value="{{$sub_category->id}}" <?php if($sub_category->id == $yacht_s_id ){ echo "selected";} ?> >{{$sub_category->category_name}}</option>

            @endforeach   

        </select>
        {!! $errors->first('sub_category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if ($type_id == 1)
<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo', 'Photo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if($yacht_photo)
            <img class="img-responsive" style="height: 60px;" src="{{ url('/public/images/photo') }}/{{$yacht->photo}}">
            {!! Form::file('photo', null, ['class' => 'form-control']) !!}
        @else
            {!! Form::file('photo', null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('protocol_file') ? 'has-error' : ''}}">
    {!! Form::label('protocol_file', 'Protocol File', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if($yacht_protocol_file)
            <img class="img-responsive" style="height: 60px;" src="{{ url('/public/images/protocol_file') }}/{{$yacht->protocol_file}}">
            {!! Form::file('protocol_file', null, ['class' => 'form-control']) !!}
        @else
            {!! Form::file('protocol_file', null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('protocol_file', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@else
@endif

<!-- <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->

<input type="hidden" value="{{$user_id}}" name="user_id">

<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('date_add') ? 'has-error' : ''}}">
    {!! Form::label('date_add', 'Date Add', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('datetime-local', 'date_add', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_add', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- <div class="form-group {{ $errors->has('date_del') ? 'has-error' : ''}}">
    {!! Form::label('date_del', 'Date Del', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('datetime-local', 'date_del', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_del', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="radio" value="1" checked name="status">Active
    <input type="radio" value="0" name="status">Inactive
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- <div class="form-group {{ $errors->has('purchase') ? 'has-error' : ''}}">
    {!! Form::label('purchase', 'Purchase', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('purchase', null, ['class' => 'form-control']) !!}
        {!! $errors->first('purchase', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('sold') ? 'has-error' : ''}}">
    {!! Form::label('sold', 'Sold', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('sold', null, ['class' => 'form-control']) !!}
        {!! $errors->first('sold', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->
<div class="form-group {{ $errors->has('year_of_build') ? 'has-error' : ''}}">
    {!! Form::label('year_of_build', 'Year Of Build', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('year_of_build', null, ['class' => 'form-control']) !!}
        {!! $errors->first('year_of_build', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('country_build') ? 'has-error' : ''}}">
    {!! Form::label('country_build', 'Country Build', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="country_build" id="" class="form-control">
            <option>--select--</option>
            @foreach ( $countries as $country )
            <option value="{{$country->id}}" <?php if($country->id == $yacht_country_build){ echo "selected"; } ?> >{{$country->country_name}}</option>
            @endforeach
        </select>
        {!! $errors->first('country_build', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('condition') ? 'has-error' : ''}}">
    {!! Form::label('condition', 'Condition', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('condition', null, ['class' => 'form-control']) !!}
        {!! $errors->first('condition', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
    {!! Form::label('location', 'Location', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('location', null, ['class' => 'form-control']) !!}
        {!! $errors->first('location', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('class') ? 'has-error' : ''}}">
    {!! Form::label('class', 'Class', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('class', null, ['class' => 'form-control']) !!}
        {!! $errors->first('class', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('imo_number') ? 'has-error' : ''}}">
    {!! Form::label('imo_number', 'Imo Number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('imo_number', null, ['class' => 'form-control']) !!}
        {!! $errors->first('imo_number', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('hp') ? 'has-error' : ''}}">
    {!! Form::label('hp', 'Hp', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('hp', null, ['class' => 'form-control']) !!}
        {!! $errors->first('hp', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('measurement') ? 'has-error' : ''}}">
    {!! Form::label('measurement', 'Measurement', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('measurement', null, ['class' => 'form-control']) !!}
        {!! $errors->first('measurement', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('length') ? 'has-error' : ''}}">
    {!! Form::label('length', 'Length', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('length', null, ['class' => 'form-control']) !!}
        {!! $errors->first('length', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('breadth') ? 'has-error' : ''}}">
    {!! Form::label('breadth', 'Breadth', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('breadth', null, ['class' => 'form-control']) !!}
        {!! $errors->first('breadth', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('depth') ? 'has-error' : ''}}">
    {!! Form::label('depth', 'Depth', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('depth', null, ['class' => 'form-control']) !!}
        {!! $errors->first('depth', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('price', null, ['class' => 'form-control']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- <div class="form-group {{ $errors->has('deleted') ? 'has-error' : ''}}">
    {!! Form::label('deleted', 'Deleted', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('deleted', null, ['class' => 'form-control']) !!}
        {!! $errors->first('deleted', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('delete_user_id') ? 'has-error' : ''}}">
    {!! Form::label('delete_user_id', 'Delete User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('delete_user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('delete_user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
